import tempfile
import unittest

from lexi.fixture import Database
from lexi.models import (Entry as LexiEntry, Headword, Metadata, Sense as LexiSense,
                         Pronunciation as LexiPronunciation)
from lexi.models.jwktl import PartOfSpeech

from digero.dictionary.make_dictionary import create_entry, make_dictionary, Entry, Sense, \
                                              HeadwordLine, Pronunciation


class MakeDictionaryTest(unittest.TestCase):

    def test_create_entry(self):
        with Database(':memory:') as db:
            db.create_tables()
            create_entry(Entry(pageTitle="test", headwordLines=[HeadwordLine("test")], partOfSpeech="Verb",
                               senses=[], categories=[], pronunciations=[], usageNotes="note"))

            entry: LexiEntry = LexiEntry.get(headword_text="test")

            self.assertEqual(entry.headword_text, "test")
            self.assertEqual(entry.usage_notes, "note")
            self.assertEqual(entry.pos, PartOfSpeech.VERB)

    def test_make_dictionary(self):
        with tempfile.NamedTemporaryFile() as file:
            make_dictionary(output=file.name,
                            dump_version='1234',
                            schema_version='5678',
                            language='de',
                            entries=[
                                Entry(pageTitle='test1', headwordLines=[HeadwordLine("test1")],
                                      partOfSpeech='Verb',
                                      senses=[
                                        Sense(definition="def1")
                                      ],
                                      categories=[],
                                      pronunciations=[Pronunciation(text='/test/')]),
                                Entry(pageTitle='test1', headwordLines=[HeadwordLine("test1")],
                                      partOfSpeech='Noun',
                                      senses=[
                                        Sense(definition="def1.1")
                                      ],
                                      categories=[],
                                      pronunciations=[]),
                                Entry(pageTitle='test2', headwordLines=[HeadwordLine("test2")],
                                      partOfSpeech='Verb', senses=[
                                        Sense(definition="def2")
                                      ],
                                      categories=[],
                                      pronunciations=[])
                            ],
                            index=True)

            with Database(file.name):
                self.assertEqual(2, Headword.select().count())
                self.assertEqual(3, LexiSense.select().count())
                self.assertEqual(1, LexiPronunciation.select().count())
                self.assertEqual(6, Metadata.select().count())
