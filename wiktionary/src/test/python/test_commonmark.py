import unittest
from digero.dictionary.commonmark import link_target


class CommonMarkTestCase(unittest.TestCase):

    def test_plain_target(self):
        self.assertEqual(('foo', 'foo'), link_target('foo'))

    def test_link(self):
        self.assertEqual(('πᾰλῠ́νω', 'παλύνω'), link_target('[πᾰλῠ́νω](./παλύνω#Ancient_Greek "παλύνω")'))

    def test_encoded_link(self):
        self.assertEqual(('ῥᾰντῐ́ζω', 'ῥαντίζω'), link_target(
            '[ῥᾰντῐ́ζω](./%E1%BF%A5%CE%B1%CE%BD%CF%84%CE%AF%CE%B6%CF%89#Ancient_Greek "ῥαντίζω")'
        ))

    def test_link_with_escaped_sequence(self):
        self.assertEqual(('m/w/*', 'm/w/*'), link_target("""[m/w/\\*](./m/w/*#German "m/w/*")"""))

    def test_list_is_returned_as_text(self):
        self.assertEqual(('5.', '5.'), link_target('5.'))

    def test_multiple_link_returns_none(self):
        self.assertIsNone(link_target('[a](./a) [b](./b)'))

    def test_mixed_link_and_text_returns_none(self):
        self.assertIsNone(link_target("""[oder](./oder#German "oder") **nicht**"""))

    def test_invalid_link_raises_exception(self):
        with self.assertRaises(Exception):
            link_target('[*a*](./a)')

    def test_emphasis_returns_none(self):
        self.assertIsNone(link_target('**a**'))


if __name__ == '__main__':
    unittest.main()
