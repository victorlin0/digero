package digero.wiktionary.tasks.dictionary

import digero.wiktionary.HTMLDumpIntegrationTest
import digero.{SparkRunner, TaskArguments}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.api.{BeforeAll, Tag, Test, TestInstance}

import java.io.File
import scala.util.Using

@Tag("integration")
@TestInstance(PER_CLASS)
class ExtractContentTest extends HTMLDumpIntegrationTest:
  var input: File = _

  @BeforeAll
  def prepareHTMLDump(): Unit =
    input = parseHTMLDump(importHTMLDump())

  @Test
  def testProcess(): Unit =
    val output = temporaryFile("content", "ndjson")

    Using.resource(SparkRunner("ExtractContent-write", null)) { runner =>
      val dataset = runner.session.read.parquet(input.getAbsolutePath)
      val arguments = TaskArguments(Seq(output.getAbsolutePath, "en"))
      ExtractContent().process(runner.session, dataset, arguments)
    }

    println(s"content written to ${output}")

    Using.resource(SparkRunner("ExtractContent-read", null)) { runner =>
      val dataset = runner.session.read.json(output.getAbsolutePath)
      assertThat(dataset.count()).isEqualTo(2)
    }


