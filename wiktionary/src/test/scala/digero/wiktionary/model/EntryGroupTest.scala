package digero.wiktionary.model

import digero.wiktionary.model.Level.{L3, L4}
import digero.wiktionary.model.SectionHeading.{Adjective, AlternativeForms, Anagrams, Declension, Definitions, Etymology, FurtherReading, Glyph, GlyphOrigin, Noun, Preposition, Pronunciation, ProperNoun, References, SeeAlso, UsageNotes, Verb}
import digero.wiktionary.model.serialization.TextConverter
import digero.wiktionary.parser.FixtureParsing
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

import scala.collection.immutable.{::, Seq}

class EntryGroupTest extends FixtureParsing:

  @Test
  def testSingleEtymology(): Unit =
    EntryGroup(parse(EntryName("simple", "en"), "parser/entry/simple_en_expected.html")) match
      case
        Entry(EntryName("simple", "en"), Some(_), pos, rest) :: Nil =>
          pos match
            case
              Section(_, L3, Some(Adjective), None, _, _) ::
              Section(_, L3, Some(Noun), None, _, _) ::
              Section(_, L3, Some(Verb), None, _, _) :: Nil =>

          rest match
            case
             Section(_, L3, Some(Pronunciation), None, _, _) ::
             Section(_, L3, Some(Anagrams), None, _, _) :: Nil =>

  @Test
  def testNoEtymology(): Unit =
    EntryGroup(parse(EntryName("played", "en"), "parser/entry/played.html")) match
      case
        Entry(EntryName("played", _), None, pos, rest) :: Nil =>
          pos match
            case Section(_, L3, Some(SectionHeading.Verb), None,  _, _) :: Nil =>
          rest match
            case Section(_, L3, Some(Pronunciation), None, _, _) ::
                 Section(_, L3, Some(SectionHeading.Anagrams), None, _, _) :: Nil =>

  @Test
  def testEtymologyNestedInPronunciation(): Unit =
    EntryGroup(parse(EntryName("bubo", "tl"), "parser/entry/tl-bubo.html")) match
      case
        Entry(_, Some(Section(_, L4, Some(Etymology), Some(1), _, _)), pos1, _) ::
        Entry(_, Some(Section(_, L4, Some(Etymology), Some(2), _, _)), pos2, _) ::
        Entry(_, None, pos3, _) ::
        Entry(_, None, pos4, _) ::
        Entry(_, Some(Section(_, L4, Some(Etymology), None, _, _)), pos5, _) :: Nil
      =>
        Seq(pos1, pos2, pos3, pos4, pos5).map(_.length) shouldEqual Seq(2, 1, 1, 1, 2)

  @Test
  def testMultipleEtymologies(): Unit =
    EntryGroup(parse(EntryName("bar", "en"), "parser/entry/bar_en_expected.html")) match
        case
          Entry(EntryName("bar", _), Some(Section(_, L3, Some(Etymology), Some(1), _, _)),
            Section(_, L4, Some(Noun), None, _, _) :: Nil,
            rest1) ::
          Entry(EntryName("bar", _), Some(Section(_, L3, Some(Etymology), Some(2), _, _)),
            Seq(
              Section(_, L4, Some(Verb), None, _, _),
              Section(_, _, Some(Preposition), None, _, _)
            ),
            rest2) ::
          Entry(EntryName("bar", _), Some(Section(_, L3, Some(Etymology), Some(3), _, _)),
            Section(_, L4, Some(Noun), None, _, _) :: Nil,
            rest3)
          :: Nil  =>
            rest1.flatMap(_.heading) shouldEqual Seq(SeeAlso, References, Pronunciation, FurtherReading, Anagrams)
            rest2.flatMap(_.heading) shouldEqual Seq(References, Pronunciation, FurtherReading, Anagrams)
            rest3.flatMap(_.heading) shouldEqual Seq(Pronunciation, FurtherReading, Anagrams)

  @Test
  def testGlyphOriginNestedInEtymology(): Unit =
    EntryGroup(parse(EntryName("白露", "ja"), "parser/entry/ja-白露.html")) match
      case
        Entry(_, Some(Section(_, L3, Some(Etymology), Some(1), _, _)), pos1, rest1) ::
        Entry(_, Some(Section(_, L3, Some(Etymology), Some(2), _, _)), pos2, rest2) ::
        Entry(_, Some(Section(_, L3, Some(Etymology), Some(3), _, _)), pos3, rest3) ::
        Entry(_, Some(Section(_, L3, Some(Etymology), Some(4), _, _)), pos4, rest4) :: Nil
      =>
        Seq(pos1, pos2, pos3, pos4).map(_.flatMap(_.heading)) shouldEqual Seq(
          Seq(ProperNoun),
          Seq(Noun),
          Seq(ProperNoun),
          Seq(ProperNoun)
        )
        Seq(rest1, rest2, rest3, rest4).map(_.flatMap(_.heading)) shouldEqual Seq(
          Seq(Pronunciation, SeeAlso),
          Seq(Pronunciation),
          Seq(Pronunciation),
          Seq(Pronunciation, GlyphOrigin)
        )

  @Test
  def testEtymologyNestedInEtymology(): Unit =
    EntryGroup(parse(EntryName("lupia", "la"), "parser/entry/la-Lupia.html")) match
      case
        Entry(_, Some(Section(_, L3, Some(Etymology), Some(1), _, _)), pos1, rest1) ::
        Entry(_, Some(Section(_, L3, Some(Etymology), Some(2), _, _)), pos2, rest2) :: Nil
      =>
        pos1.flatMap(_.heading) shouldEqual Seq(ProperNoun)
        pos2.flatMap(_.heading) shouldEqual Seq(ProperNoun)
        rest1.flatMap(_.heading) shouldEqual Seq(AlternativeForms, References, Etymology /* duplicated */)
        rest2.flatMap(_.heading) shouldEqual Seq(Pronunciation, Declension)

  @Test
  def testMultipleGlyphHeader(): Unit =
    given TextConverter = (s: String) => Some(s)
    
    EntryGroup(parse(EntryName("劄", "zh"), "parser/entry/zh-劄.html")) match
      case
        Entry(_, Some(Section(_, L4, Some(Etymology), Some(1), _, _)), pos1, rest1) ::
        Entry(_, Some(Section(_, L4, Some(Etymology), Some(2), _, _)), pos2, rest2) ::
        Entry(_, Some(Section(_, L4, Some(Etymology), Some(3), _, _)), pos3, rest3) ::
        (entry4 @ Entry(_, Some(Section(_, L3, Some(Glyph), Some(2), _, _)), pos4, rest4)) :: Nil
      =>
          pos1.flatMap(_.heading) shouldEqual Seq(Definitions)
          rest1.flatMap(_.heading) shouldEqual Seq(UsageNotes, GlyphOrigin, Glyph)

          pos2.flatMap(_.heading) shouldEqual Seq(Definitions)
          rest2.flatMap(_.heading) shouldEqual Seq(UsageNotes, References, GlyphOrigin, Glyph)

          pos3.flatMap(_.heading) shouldEqual Seq(Definitions)
          rest3.flatMap(_.heading) shouldEqual Seq(Pronunciation, GlyphOrigin, Glyph)

          pos4.flatMap(_.heading) shouldEqual Seq(Definitions)
          rest4.flatMap(_.heading) shouldEqual Seq(Pronunciation, References, GlyphOrigin)

          entry4.partsOfSpeech.map(_.senses).foldLeft(Seq[Sense]())(_ ++ _) should have length 3