package digero.wiktionary.model.serialization

import digero.wiktionary.model.Sense
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import scala.util.Using

class SerializedEntryTest:
  given TextConverter = (s: String) => Some(s"H: $s")

  @Test
  def testAsJSON(): Unit =
    val entry = SerializedEntry(
      pageTitle = "foo",
      headwordLines = Seq.empty,
      partOfSpeech = "Noun",
      senses = Seq(
        SerializedSense((1, 0, 0, 0), "sense1", Seq.empty, None),
        SerializedSense((2, 0, 0, 0), "sense2", Seq.empty, None),
        SerializedSense((2, 1, 0, 0), "subsense 2.1", Seq.empty, None),
      ),
      etymology = Some("etymology"),
      pronunciations = Seq.empty,
      translations = Seq.empty,
      usageNotes = None,
      categories = Seq.empty
    )
    CustomPickler.write(entry, indent = 2) shouldEqual
      """{
        |  "pageTitle": "foo",
        |  "headwordLines": [],
        |  "partOfSpeech": "Noun",
        |  "senses": [
        |    {
        |      "number": [
        |        1,
        |        0,
        |        0,
        |        0
        |      ],
        |      "definition": "sense1",
        |      "relations": [],
        |      "translationGloss": null
        |    },
        |    {
        |      "number": [
        |        2,
        |        0,
        |        0,
        |        0
        |      ],
        |      "definition": "sense2",
        |      "relations": [],
        |      "translationGloss": null
        |    },
        |    {
        |      "number": [
        |        2,
        |        1,
        |        0,
        |        0
        |      ],
        |      "definition": "subsense 2.1",
        |      "relations": [],
        |      "translationGloss": null
        |    }
        |  ],
        |  "etymology": "etymology",
        |  "pronunciations": [],
        |  "translations": [],
        |  "usageNotes": null,
        |  "categories": []
        |}""".stripMargin

  @Test
  def testAsJSONWithNull(): Unit =
    val entry = SerializedEntry(
      pageTitle = "foo",
      partOfSpeech = "Noun",
      headwordLines = Seq.empty,
      senses = Seq.empty,
      etymology = None,
      pronunciations = Seq.empty,
      translations = Seq.empty,
      usageNotes = None,
      categories = Seq.empty)

    CustomPickler.write(entry) shouldEqual
      """{"pageTitle":"foo","headwordLines":[],"partOfSpeech":"Noun","senses":[],"etymology":null,"pronunciations":[],"translations":[],"usageNotes":null,"categories":[]}"""

  @Test def testSerialized(): Unit =
    val senses = Seq[Sense](
      Sense("1", html = "1",
        subsenses = Seq(
          Sense("1.1", html = "1.1"),
          Sense("1.2", html = "1.2")
        )
      ),
      Sense("2", html = "2")
    )

    senses.serialized shouldEqual Seq(
      SerializedSense((1, 0, 0, 0), "H: 1", Seq.empty, None),
      SerializedSense((1, 1, 0, 0), "H: 1.1", Seq.empty, None),
      SerializedSense((1, 2, 0, 0), "H: 1.2", Seq.empty, None),
      SerializedSense((2, 0, 0, 0), "H: 2", Seq.empty, None)
    )

  @Test def testJavaSerialization(): Unit =
    val entry = SerializedEntry(
      pageTitle = "foo",
      partOfSpeech = "Noun",
      headwordLines = Seq(SerializedHeadwordLine("foo", None, Seq.empty, Seq.empty)),
      senses = Seq(SerializedSense((0, 0, 0, 0), "definition", Seq.empty, None)),
      etymology = None,
      pronunciations = Seq(SerializedPronunciation("foo", Seq.empty)),
      translations = Seq(
        SerializedTranslationGroup(
          "gloss",
          Seq(SerializedTranslation("foo", "en", None)),
          None)
      ),
      usageNotes = Some("usage"),
      categories = Seq.empty
    )

    val byteArrayOutputStream = ByteArrayOutputStream()
    val objectOutputStream = ObjectOutputStream(byteArrayOutputStream)
    objectOutputStream.writeObject(entry)
    objectOutputStream.close()

    val deserializedEntry = Using(ObjectInputStream(
      new ByteArrayInputStream(byteArrayOutputStream.toByteArray))) { is =>
      is.readObject().asInstanceOf[SerializedEntry]
    }.get

    deserializedEntry shouldEqual entry

    
