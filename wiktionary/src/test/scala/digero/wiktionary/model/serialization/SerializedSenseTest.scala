package digero.wiktionary.model.serialization

import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class SerializedSenseTest:

  @Test def testAsJSON(): Unit =
    val sense = SerializedSense(number=(0, 0, 0, 0), "definition", Seq.empty, Some("translation"))
    CustomPickler.write(sense) should equal(
      """{"number":[0,0,0,0],"definition":"definition","relations":[],"translationGloss":"translation"}"""
    )

  @Test def testAsJSON_NoGloss(): Unit =
    val sense = SerializedSense(number=(0, 0, 0, 0), "definition", Seq.empty, None)
    CustomPickler.write(sense) should equal(
      """{"number":[0,0,0,0],"definition":"definition","relations":[],"translationGloss":null}"""
    )