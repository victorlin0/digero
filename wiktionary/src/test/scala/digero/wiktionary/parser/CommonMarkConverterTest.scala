package digero.wiktionary.parser

import com.vladsch.flexmark.html.renderer.{LinkType, ResolvedLink}
import digero.test.Fixture
import digero.wiktionary.parser.CustomLinkResolverTest.{blueLink, redLink, wikipediaLink}
import org.jsoup.Jsoup
import org.scalatest.matchers.should.Matchers.*
import org.junit.jupiter.api.{BeforeEach, Test}

class CommonMarkConverterTest extends Fixture:
  var subject: CommonMarkConverter = _

  val convert: String => String = subject.convert(_).getOrElse("")

  @BeforeEach
  def setup(): Unit =
    subject = CommonMarkConverter()

  @Test def convertLink(): Unit =
    convert(
      """
      <a href="foo">bar</a>
      """) shouldEqual "[bar](foo)"

  @Test def convertWiktionaryLink(): Unit =
    convert(
      """
        <a rel="mw:WikiLink" href="./ἱππομανές#Ancient_Greek" title="ἱππομανές">ἱππομᾰνές</a>
      """) shouldEqual """[ἱππομᾰνές](./ἱππομανές#Ancient_Greek "ἱππομανές")"""

  @Test def keepsEnDashesIntact(): Unit =
    convert("–") shouldEqual "–"

  @Test def keepsEmDashesIntact(): Unit =
      convert("—") shouldEqual "—"

  @Test def keepsQuotesIntact(): Unit =
    convert("“”‘’«»") shouldEqual "“”‘’«»"

  @Test def replaceQuoteEntities(): Unit =
    convert("&hellip;") shouldEqual "…"
    convert("&laquo;") shouldEqual "«"
    convert("&raquo;") shouldEqual "»"
    convert("&lsquo;") shouldEqual "‘"
    convert("&rsquo;") shouldEqual "’"
    convert("&ldquo;") shouldEqual "“"
    convert("&rdquo;") shouldEqual "”"
    convert("&apos;") shouldEqual "'"
    convert("&endash;") shouldEqual "–"
    convert("&emdash;") shouldEqual "—"

  @Test def convertEtymology(): Unit =
    val result = convert(fixture("converter/foo_etymology.html"))
//    fixtureWrite(content = result, "foo_etymology.md")
    result shouldEqual fixture("converter/foo_etymology.md")

  @Test def itStripsTheFirstH3Heading(): Unit =
    convert(
      """
        <h3 id="Etymology">Etymology</h3
        <a href="foo">bar</a>
      """) shouldEqual "[bar](foo)"

  @Test def itStripsTheFirstH4Heading(): Unit =
    convert(
      """
        <h4 id="Etymology">Etymology</h4
        <a href="foo">bar</a>
      """) shouldEqual "[bar](foo)"

  @Test def itRemovesNewlinesAroundIgnoredElements(): Unit =
    convert(
      """
        <p id="mwKA">From <i class="Latn mention" lang="ga" about="#mwt13" typeof="mw:Transclusion" data-mw="" id="mwKQ"><a rel="mw:WikiLink" href="./fianna#Irish" title="fianna">fianna</a></i><span about="#mwt13"> </span><span class="mention-gloss-paren annotation-paren" about="#mwt13">(</span><span class="ann-pos" about="#mwt13">plural of <i class="Latn mention" lang="ga"><a rel="mw:WikiLink" href="./fiann#Irish" title="fiann">fiann</a></i> <span class="mention-gloss-paren annotation-paren">(</span><span class="mention-gloss-double-quote">“</span><span class="mention-gloss">warrior</span><span class="mention-gloss-double-quote">”</span><span class="mention-gloss-paren annotation-paren">)</span></span><span class="mention-gloss-paren annotation-paren" about="#mwt13">)</span><span about="#mwt13"> +</span><span typeof="mw:Entity" about="#mwt13">‎</span><span about="#mwt13"> </span><i class="Latn mention" lang="ga" about="#mwt13"><a rel="mw:WikiLink" href="./Fáil?action=edit&amp;redlink=1#Irish" title="Fáil" class="new" typeof="mw:LocalizedAttrs" data-mw-i18n="">Fáil</a></i><span about="#mwt13"> </span><span class="mention-gloss-paren annotation-paren" about="#mwt13">(</span><span class="ann-pos" about="#mwt13">genitive of <i class="Latn mention" lang="ga"><a rel="mw:WikiLink" href="./Fál?action=edit&amp;redlink=1#Irish" title="Fál" class="new" typeof="mw:LocalizedAttrs" data-mw-i18n="">Fál</a></i> <span class="mention-gloss-paren annotation-paren">(</span><span class="ann-pos">a legendary name of Ireland</span><span class="mention-gloss-paren annotation-paren">)</span></span><span class="mention-gloss-paren annotation-paren" about="#mwt13">)</span>
            <link rel="mw:PageProp/Category" href="./Category:Irish_compound_nouns#FIANNA%20FAIL" about="#mwt13" id="mwKg">. The original meaning is thus "warriors of Fál", but the phrase is traditionally translated "soldiers of destiny".
        </p>
      """) shouldNot contain('\n')

  @Test def itStripsMaintenance(): Unit =
    convert(
      """
        <p id="mwCA"><span class="maintenance-line" style="color: #777777;" about="#mwt2" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;rfe&quot;,&quot;href&quot;:&quot;./Template:rfe&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;ga&quot;}},&quot;i&quot;:0}}]}" id="mwCQ">(This <a rel="mw:WikiLink" href="./Wiktionary:Etymology" title="Wiktionary:Etymology">etymology</a> is missing or incomplete. Please add to it, or discuss it at the <a rel="mw:WikiLink" href="./Wiktionary:Etymology_scriptorium" title="Wiktionary:Etymology scriptorium">Etymology scriptorium</a>.)</span>  <link rel="mw:PageProp/Category" href="./Category:Requests_for_etymologies_in_Irish_entries#ANFAIS" about="#mwt2" id="mwCg"></p>
      """) shouldBe(empty)

object CustomLinkResolverTest:
  val blueLink =
    """
    <a rel="mw:WikiLink" href="./stain" title="stain" id="mwFA">stains</a>
    """

  val redLink =
    """
    <a rel="mw:WikiLink" href="./emaculatus?action=edit&amp;redlink=1#Latin" title="emaculatus" class="new" typeof="mw:LocalizedAttrs" data-mw-i18n="{&quot;title&quot;:{&quot;lang&quot;:&quot;x-page&quot;,&quot;key&quot;:&quot;red-link-title&quot;,&quot;params&quot;:[&quot;emaculatus&quot;]}}">emaculatus</a>
    """

  val wikipediaLink =
    """
    <a rel="mw:WikiLink/Interwiki" href="https://en.wikipedia.org/wiki/John%20Hales" title="w:John Hales" about="#mwt6" class="extiw">John Hales</a>
    """

class CustomLinkResolverTest:
  private def resolve(html: String): ResolvedLink =
    val node = Jsoup.parse(html)
    val url = node.selectFirst("a").attr("href")
    val link = new ResolvedLink(LinkType.LINK, url)
    customLinkResolver.resolveLink(node, null, link)

  @Test def testResolveBlueLink(): Unit =
    val resolved = resolve(blueLink)
    resolved.getUrl shouldEqual "./stain"

  @Test def testResolveRedLinkRemovesQueryParameter(): Unit =
    val resolved = resolve(redLink)
    resolved.getUrl shouldEqual "./emaculatus#Latin"

  @Test def testResolveWikipediaLink(): Unit =
    val resolved = resolve(wikipediaLink)
    resolved.getUrl shouldEqual "https://en.wikipedia.org/wiki/John%20Hales"