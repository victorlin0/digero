package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.lexical.{Pronunciation, PronunciationType}
import org.junit.jupiter.api.{BeforeEach, Test}

import scala.collection.immutable.::

class PronunciationParserTest extends Fixture:
  var subject: PronunciationParser = _

  @BeforeEach
  def prepare(): Unit = subject = PronunciationParser()

  @Test
  def testParseEmpty(): Unit =
    subject.parse("") match
      case Nil =>

  @Test
  def testParseIPA(): Unit =
    subject.parse("""
        |<span class="IPA">/foo/</span>
        |<span class="IPA">/bar/</span>
        |""".stripMargin) match
      case Pronunciation("/foo/", PronunciationType.IPA, Seq()) ::
           Pronunciation("/bar/", PronunciationType.IPA, Seq()) :: Nil =>

  @Test
  def testSkipsLinkedIPA(): Unit =
    subject.parse("""
        |<span class="IPA">/foo/</span>
        |<a href="link"><span class="IPA">/link/</span></a>
        |""".stripMargin) match
      case Pronunciation("/foo/", PronunciationType.IPA, Seq()) :: Nil =>

  @Test
  def testParsesAccentQualifiersCatalan(): Unit =
    subject.parse(fixture("parser/pronunciation/ca-IPA.html")) match
      case Pronunciation("/ˈka.zə/", PronunciationType.IPA, Seq("Balearic", "Central")) ::
           Pronunciation("/ˈka.za/", PronunciationType.IPA, Seq("Valencian")) :: Nil =>

  @Test
  def testParsesAccentQualifiersSpanish(): Unit =
    subject.parse(fixture("parser/pronunciation/es-IPA.html")) match
      case Pronunciation("/ˈθeɾo/", PronunciationType.IPA, Seq("Castilian")) ::
           Pronunciation("/ˈseɾo/", PronunciationType.IPA, Seq("Latin America")) :: Nil =>

  @Test
  def testParsesEsperanto(): Unit =
    subject.parse(fixture("parser/pronunciation/eo-IPA.html")) match
      case Pronunciation("/t͡ʃevaˈlido/", PronunciationType.IPA, Seq()) :: Nil =>

  @Test
  def testParsesItalian(): Unit =
    subject.parse(fixture("parser/pronunciation/it-IPA.html")) match
      case Pronunciation("/inˈsje.me/", PronunciationType.IPA, Seq()) :: Nil =>

  @Test
  def testParsesKorean(): Unit =
    subject.parse(fixture("parser/pronunciation/ko-IPA.html")) match
      case Pronunciation("[ˈkɥiːjʌ̹p̚t͈a̠] ~ [ˈkyːjʌ̹p̚t͈a̠]", PronunciationType.IPA, Seq()) :: Nil =>


  @Test
  def testParsesLatin(): Unit =
    subject.parse(fixture("parser/pronunciation/la-IPA.html")) match
      case Pronunciation("/ˈwe.ni.oː/", PronunciationType.IPA, Seq("Classical")) ::
           Pronunciation("[ˈwɛ.n̪i.oː]", PronunciationType.IPA, Seq("Classical")) ::
           Pronunciation("/ˈve.ni.o/", PronunciationType.IPA, Seq("Ecclesiastical")) ::
           Pronunciation("[ˈvɛː.ni.ɔ]", PronunciationType.IPA, Seq("Ecclesiastical")) :: Nil =>

  @Test
  def testParsesRussian(): Unit =
    subject.parse(fixture("parser/pronunciation/ru-IPA.html")) match
      case Pronunciation("[ʐːom]", PronunciationType.IPA, Seq()) ::
           Pronunciation("[ʑːɵm]", PronunciationType.IPA, Seq()) :: Nil =>
