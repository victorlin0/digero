package digero.wiktionary.parser;

import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

class CommonMarkParserTest:
  var subject: CommonMarkParser = _

  @BeforeEach
  def before(): Unit = subject = CommonMarkParser()

  @Test def convertToPlain(): Unit =
    subject.convertToPlain(
      """
      ([mineralogy](./mineralogy "mineralogy")) [Homogenous](./homogenous "homogenous").
      """.strip()) should equal("(mineralogy) Homogenous.")

  @Test def convertToPlain2(): Unit =
    subject.convertToPlain(
      """
      ([pathology](./pathology "pathology")) A condition characterized by unequal size of the [pupils](./pupil "pupil") of the [eye](./eye "eye")
      """.strip()) should equal("(pathology) A condition characterized by unequal size of the pupils of the eye")


  @Test def simplifyDifferentTitleReturnsFullLink(): Unit =
    subject.simplify(
      """
      [ἱππομᾰνές](./ἱππομανές#Ancient_Greek "ἱππομανές")
      """.strip(), "Ancient_Greek"
    ) should equal("""[ἱππομᾰνές](./ἱππομανές#Ancient_Greek "ἱππομανές")""")

  @Test def simplifySameTitleReturnsJustText(): Unit =
    subject.simplify(
      """
      [foo](./foo#English "foo")
      """.strip(), "English"
    ) should equal("foo")

  @Test def simplifyDifferentLanguageIsUnchanged(): Unit =
    subject.simplify(
      """
      [foo](./foo#English "foo")
      """.strip(), "French"
    ) should equal("""[foo](./foo#English "foo")""")

  @Test def simplifySameTitleWithAdditionalTextIsUnchanged(): Unit =
    subject.simplify(
      """
      [foo](./foo#English "foo") Something else
      """.strip(), "English"
    ) should equal("""[foo](./foo#English "foo") Something else""")

  @Test def simplifyMultipleLinksIsUnchanged(): Unit =
    subject.simplify(
      """
      [foo](./foo#English "foo") [bar](./bar#English "bar")
      """.strip(), "English"
    ) should equal("""[foo](./foo#English "foo") [bar](./bar#English "bar")""")