package digero.wiktionary.parser

import digero.wiktionary.parser.HTMLPreprocessor.preprocess
import digero.wiktionary.parser.JsoupExtensions.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class HTMLPreprocessorTest:

  @Test def itRemovesUnwantedAttributes(): Unit =
    val input = """<span class="etyl" about="#mwt18" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;der&quot;,&quot;href&quot;:&quot;./Template:der&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;en&quot;},&quot;2&quot;:{&quot;wt&quot;:&quot;zh&quot;},&quot;3&quot;:{&quot;wt&quot;:&quot;福&quot;},&quot;4&quot;:{&quot;wt&quot;:&quot;&quot;},&quot;5&quot;:{&quot;wt&quot;:&quot;[[fortunate]]; [[prosperity]], [[good]] [[luck]]&quot;},&quot;tr&quot;:{&quot;wt&quot;:&quot;fú&quot;}},&quot;i&quot;:0}}]}" id="mwMQ">"""
    assertThat(preprocess(input).serialize).isEqualTo("""<span class="etyl" typeof="mw:Transclusion"></span>""")

  @Test def itStripsCategoryLinks(): Unit =
    val input = """<link rel="mw:PageProp/Category" href="./Category:Terms_with_manual_transliterations_different_from_the_automated_ones#FOO" about="#mwt19">"""
    assertThat(preprocess(input).serialize).isEmpty()

  @Test def itStripsReferenceLists(): Unit =
    val input = "<span>foo</span><div class=\"mw-references-wrap\"><ol class=\"references\"></ol></div>"
    assertThat(preprocess(input).serialize).isEqualTo("<span>foo</span>")

  /*
  @Test def itStripsCiteRefs(): Unit =
    val input = "<p>Hello<a href=\"#cite_ref-foo\">1</a></p>"
    assertThat(preprocess(input)).isEqualTo("<p>Hello</p>")
   */

  @Test def colorGetsStripped(): Unit =
    assertThat(preprocess("<div class=\"color-panel\"><table></table></div>").serialize).isEmpty()

  @Test def testMaintenanceLineGetStripped(): Unit =
    assertThat(preprocess("<span class=\"maintenance-line\">foo</span").serialize).isEmpty()

  @Test def testMaintenanceBoxGetStripped(): Unit =
    assertThat(preprocess("<span class=\"maintenance-box\">foo</span").serialize).isEmpty()

  @Test def testRequestBoxGetStripped(): Unit =
    assertThat(preprocess("<div class=\"request-box\">foo</div").serialize).isEmpty()

  @Test def testAttentionGetStripped(): Unit =
    assertThat(preprocess("<span class=\"attentionseeking\">Attention!</span").serialize).isEmpty()

  @Test def filtersSisterProjects(): Unit =
    val input = "Foo<div class=\"sister-wikipedia sister-project noprint floatright\" style=\"border: 1px solid #aaa; font-size: 90%; background: #f9f9f9; width: 250px; padding: 4px; text-align: left;\">\n" + "</div>Baz"
    assertThat(preprocess(input).serialize).isEqualTo("FooBaz")

  @Test def filtersFigureTags(): Unit =
    assertThat(preprocess("<figure class=\"mw-default-size\" typeof=\"mw:Error mw:Image/Thumb\"></figure>").serialize).isEmpty()

  @Test def filtersAudioTables(): Unit =
    assertThat(preprocess("<table class=\"audiotable\" style=\"vertical-align: bottom; display:inline-block; list-style:none;line-height: 1em; border-collapse:collapse;\">\n" +
        "<tbody></tbody></table>").serialize).isEmpty()

  @Test def filtersNumberBoxes(): Unit =
    assertThat(preprocess(
      """
        |<table class="floatright number-box" cellpadding="5" cellspacing="0" rules="all">
        |<caption><b>Irish cardinal numbers</b>
        |</caption>
        |<tbody><tr>
        |<td class="adjacent-slot"><span class="None" lang="ga">&nbsp;&lt;&nbsp;&nbsp;999</span>
        |</td>
        |<th class="current-slot"><span class="None" lang="ga">1000</span>
        |</th>
        |<td class="adjacent-slot"><span class="None" lang="ga">1001&nbsp;&nbsp;&gt;&nbsp;</span>
        |</td></tr>
        |<tr>
        |<td colspan="3" class="form-slot">&nbsp;&nbsp;&nbsp; <i><a href="/wiki/cardinal_number" title="cardinal number">Cardinal</a></i>&nbsp;: <span class="Latn" lang="ga"><strong class="selflink">míle</strong></span><br> &nbsp;&nbsp;&nbsp; <i><a href="/wiki/ordinal_number" title="ordinal number">Ordinal</a></i>&nbsp;: <span class="Latn" lang="ga"><a href="/w/index.php?title=m%C3%ADli%C3%BA&amp;action=edit&amp;redlink=1" class="new" title="míliú (page does not exist)">míliú</a></span>
        |</td></tr>
        |<tr>
        |<td colspan="3" class="footer-slot">
        |</td></tr></tbody></table>
        |""".stripMargin).serialize).isBlank()

  @Test def filtersAbbrTags(): Unit =
    assertThat(preprocess("<abbr>bar baz</abbr>").serialize).isEmpty()


  @Test def filtersMultipleImageTags(): Unit =
    // https://en.wiktionary.org/wiki/Module:multiple_images
    assertThat(preprocess("<div class=\"thumb tmulti tright\">Some content</div>").serialize).isEmpty()

  @Test def removesWhitespaceAroundIgnoredElements(): Unit =
    assertThat(preprocess(
      """
        |<p>Foo<span/>
        |   <link rel="mw:PageProp/Category">.</p>
      """.stripMargin).serialize.strip()).isEqualTo("<p>Foo<span></span>.</p>")
