package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.parser.JsoupExtensions.*
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*


class JsoupExtensionsTest extends Fixture with HTMLParser:
  @Test def testParsoidData(): Unit =
    val document = parseDocument(fixture("parser/entry/water.html"))
    for
      element <- document.select("*[data-mw]")
      mwData <- element.attrOpt("data-mw")
    do
      withClue(mwData) { element.parsoidData should not be empty }


  @Test def testTemplates(): Unit =
    val document = parseDocument(fixture("parser/headword/ko-noun.html"))
    val strong = document.selectFirst("strong")
    strong.templates should not be empty
    strong.templates.map(_.name) should contain("ko-noun")
    strong.templates.flatMap(_.parameters) should contain(Right("hanja") -> "[[椰子樹]]")