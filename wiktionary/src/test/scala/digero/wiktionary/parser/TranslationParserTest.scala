package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.serialization.TextConverter
import org.scalatest.matchers.should.Matchers.*
import org.junit.jupiter.api.{BeforeEach, Test}
import digero.wiktionary.model.{EntryName, Translation, TranslationGroup, TranslationSubpage}

class TranslationParserTest extends Fixture:
  var subject: TranslationParser = _

  given TextConverter = CommonMarkConverter().convert
  given EntryContext(EntryName("test", "en"))

  @BeforeEach
  def prepare(): Unit = subject = TranslationParser()

  @Test
  def testParseTranslationSectionWithLinkToSubpage(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/translation_section_link_to_subpage.html")
    )

    translations match
      case TranslationGroup("", Nil, Some(TranslationSubpage("test", Some("Noun")))) :: Nil =>

  @Test
  def testParseTranslationSection(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/simple_translation_section.html")
    )
    translations match
      case TranslationGroup(gloss1, translations1, None) ::
           TranslationGroup(gloss2, translations2, None) :: Nil =>

          gloss1 should equal("uncomplicated")
          gloss2 should equal("simple-minded")

          translations1 should have length 174
          translations2 should have length 31

          translations1.take(3) match
            case Translation("af", "eenvoudig", None, "Latn", _, false) ::
                 Translation("sq", "thjeshtë", None, "Latn", _, false) ::
                 Translation("ar", "بَسِيط", Some("basīṭ"), "Arab", _, false) :: Nil =>

          translations1.reverse.take(3) match
            case Translation("zza", "gerges", None, "Latn", _, false) ::
                 Translation("yi", "פּשוט", Some("poshet"), "Hebr", _, false) ::
                 Translation("fy", "ienfâldich", None, "Latn", _, false) :: Nil =>

          translations2.take(3) match
            case Translation("hy", "պարզամիտ", Some("parzamit"), "Armn", _, false) ::
                 Translation("hy", "միամիտ", Some("miamit"), "Armn", _, false) ::
                 Translation("bg", "наивен", Some("naiven"), "Cyrl", _, false) :: Nil =>

          translations2.reverse.take(3) match
            case Translation("sv", "enkel", None, "Latn", _, false) ::
                 Translation("sh", "prostodušan", None, "Latn", _, false) ::
                 Translation("sh", "простодушан", None, "Cyrl", _, false) :: Nil =>

  @Test
  def testParseNonIdiomaticTranslation(): Unit =
    val translations = subject.parseTranslationSection(
        fixture("parser/translation/high_island.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language == "de") should contain only
      Translation("de", "Vulkaninsel", None, "Latn", None, false)

    translations.head.translations.filter(_.language == "it") should contain only
      Translation("it", "[isola](./isola#Italian \"isola\") [vulcanica](./vulcanica#Italian \"vulcanica\")", None, "Latn", None, false)

  @Test
  def testParseTranslationWithAttention(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/penumbra.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language == "he") should contain only
      Translation("he", "פְּלַג צֵל", None, "Hebr", None, false)

  @Test
  def testParseTranslationChineseVariants(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/Cagayan.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language.startsWith("zh")) should contain only(
      Translation("zh-Hans", "卡加延德奥罗", None, "Hans", None, false),
      Translation("zh-Hant", "卡加延德奧羅", None, "Hant", None, false)
    )

  @Test
  def testParseTranslationIgnoresEmptyLinks(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/Cor_Caroli.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language == "fr") shouldBe empty
    translations.head.translations.filter(_.language == "it") shouldBe empty

  @Test
  def testSkipTranslationToBeChecked(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/to_be_checked.html")
    )

    translations.map(_.gloss) should contain only "with great ability"

  @Test
  def testGlossWithMarkup(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/libre.html")
    )

    translations.map(_.gloss) should contain only "(software): with very few limitations on distribution or improvement"
