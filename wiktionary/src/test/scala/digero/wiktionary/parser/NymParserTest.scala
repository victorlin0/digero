package digero.wiktionary.parser;

import digero.test.Fixture
import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.RelationType.{AlternativeForm, CoordinateTerm, Synonym}
import digero.wiktionary.model.lexical.{Nym, RelationType}
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

class NymParserTest extends Fixture:
  var subject: NymParser = _

  given EntryContext(EntryName("source", "en"))

  @BeforeEach
  def prepare(): Unit = subject = NymParser()

  @Test
  def parseSynonyms(): Unit =
    val nyms = subject.parse(fragment("parser/nym/synonym.html"))
    nyms should contain inOrderOnly (
      Nym(Synonym, "source", "baz"),
      Nym(Synonym, "source", "bazz")
    )

  @Test
  def parseCoordinateTerm(): Unit =
    val nyms = subject.parse(fragment("parser/nym/cot.html"))
    nyms should contain only Nym(CoordinateTerm, "source", "cot")

  @Test
  def parseAlternativeForm(): Unit =
    val nyms = subject.parse(fragment("parser/nym/alti.html"))
    nyms should contain only Nym(AlternativeForm, "source", "alt")

  @Test
  def parseDifferentLinkTargets(): Unit =
    given EntryContext(EntryName("στρύχνον", "grc"))

    val nyms = subject.parse(fragment("parser/nym/synonym_different_link_target.html"))
    nyms should contain only Nym(Synonym, "στρύχνον", "[ἱππομᾰνές](./ἱππομανές#Ancient_Greek \"ἱππομανές\")")

  private def fragment(fixturePath: String): Element =
    Jsoup.parseBodyFragment(fixture(fixturePath)).body().firstElementChild()