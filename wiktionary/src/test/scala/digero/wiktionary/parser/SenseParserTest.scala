package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.lexical.RelationType.{Antonym, Synonym}
import digero.wiktionary.model.lexical.{Nym, RelationType}
import digero.wiktionary.model.{EntryName, Sense, SenseKind}
import org.assertj.core.api.Assertions.{assertThat, fail}
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

class SenseParserTest extends Fixture:
  var subject: SenseParser = _

  @BeforeEach
  def prepare(): Unit = subject = SenseParser()

  @Test
  def testParseFormOf(): Unit =
      given EntryContext(EntryName("played", "en"))

      subject.parse(fixture("parser/sense/played_senses.html")).head match
        case Sense("simple past tense and past participle of play", _, kind, Seq(), Seq()) if kind.contains(SenseKind.FormOf) =>

  @Test
  def testEmptySensesAreSkipped(): Unit =
    given EntryContext(EntryName("crab", "en"))

    val result = subject.parse(fixture("parser/sense/crab_senses.html"))
    result should have length(10)
    result.head.text should startWith("A crustacean ")

  @Test
  def testParse_simple(): Unit =
    given EntryContext(EntryName("simple", "en"))

    val result = subject.parse(fixture("parser/sense/simple_senses.html"))
    assertThat(result.size).isEqualTo(8)
    result.zipWithIndex.foreach((sense, index) => print(f"${index + 1} $sense"))

    result match
      case Sense("Uncomplicated; taken by itself, with nothing added.", _, _, _, Seq()) ::
           Sense("Without ornamentation; plain.", _, _, _, Seq()) ::
           Sense("Free from duplicity; guileless, innocent, straightforward.", _, _, _, Seq()) ::
           Sense("Undistinguished in social condition; of no special rank.", _, _, Seq(Nym(Antonym, "simple", "gentle")), Seq()) ::
           Sense("(now rare ) Trivial; insignificant.", _, _, _, Seq()) ::
           Sense("(now colloquial , euphemistic ) Feeble-minded; foolish.", _, _, _, Seq()) ::
           Sense("(heading, technical) Structurally uncomplicated.", _, _, _,
              Sense("(chemistry , pharmacology ) Consisting of one single substance; uncompounded.", _, _, _, Seq()) ::
              Sense("(mathematics ) Of a group: having no normal subgroup.", _, _, _, Seq()) ::
              Sense("(botany ) Not compound, but possibly lobed.", _, _, _, Seq()) ::
              Sense("(of a steam engine) Using steam only once in its cylinders, in contrast to a compound engine, where steam is used more than once in high-pressure and low-pressure cylinders.", _, _, _, Seq()) ::
              Sense("(zoology ) Consisting of a single individual or zooid; not compound.", _, _, _, Seq()) ::
              Sense("(mineralogy ) Homogenous.", _, _, _, Seq()) :: Nil)
           ::
           Sense("(obsolete ) Mere; not other than; being only.", _, _, _, Seq())
           :: Nil =>

      case e => fail(s"not matched $e")

  @Test
  def testParse_στρύχνον(): Unit =
    given EntryContext(EntryName("στρύχνον", "grc"))
    val result = subject.parse(fixture("parser/sense/στρύχνον_senses.html"))

    result match
      case Sense("name of various nightshades:", _, _, Seq(),
        Sense("winter cherry (Alkekengi officinarum, syn. Physalis alkekengi)", _, _, nyms1, Seq()) ::
        Sense("black nightshade (Solanum nigrum)", _, _, Seq(), Seq()) ::
        Sense("thorn apple (Datura stramonium)", _, _, nyms2, Seq()) ::
        Sense("ashwagandha (Withania somnifera)", _, _, nyms3, Seq()) :: Nil
      ) :: Nil =>

        nyms1 should contain(Nym(Synonym, "στρύχνον", "ἁλικάκκαβον"))
        nyms2 should contain(Nym(Synonym, "στρύχνον", "[ἱππομᾰνές](./ἱππομανές#Ancient_Greek \"ἱππομανές\")"))
        nyms3 should contain inOrderOnly (Nym(Synonym, "στρύχνον", "ἁλικάκκαβον"),
                                          Nym(Synonym, "στρύχνον", "κακκαλία"),
                                          Nym(Synonym, "στρύχνον", "μώριος"))

      case e => fail(s"not matched $e")
