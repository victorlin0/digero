#!/usr/bin/env python
from digero.publish.wiktionary import upload
from digero.publish.tables import make_tsv, make_language_index
from glob import glob
from os import makedirs
from os.path import dirname, join, expanduser, exists, split
from shutil import copyfileobj
from ipanema import query_language
import bz2
import tomllib


def publish_overview_table(language_data, wiktionary_wanted_page, dump_version):
    overview_table = make_language_index(sorted(language_data, reverse=True))
    overview_page = '/'.join([wiktionary_wanted_page, dump_version])
    latest_overview_page = '/'.join([wiktionary_wanted_page, 'latest'])

    upload(page=overview_page,
           text=overview_table,
           summary='overview table',
           redirect_page=latest_overview_page)


def publish_language(file, canonical_name, lang_code, wiktionary_wanted_page,
                     wiktionary_wanted_template, dump_version):
    lang_page = '/'.join([wiktionary_wanted_page, dump_version, lang_code])
    latest_page = '/'.join([wiktionary_wanted_page, 'latest', lang_code])
    data_page = lang_page + '/data'

    upload(page=data_page,
           text='<pre><nowiki>\n%s\n</nowiki></pre>' % make_tsv(file, lang_code),
           summary='data for %s' % canonical_name)

    upload(page=lang_page,
           text=wiktionary_wanted_template,
           summary='module for %s' % canonical_name,
           redirect_page=latest_page)


def archive_data(file, target_dir, lang_code, dump_version):
    wanted_dir = join(target_dir, dump_version, 'wanted')
    makedirs(wanted_dir, exist_ok=True)
    with open(file, 'rb') as src:
        with bz2.BZ2File(join(wanted_dir, '%s.jsonl.bz2' % lang_code), 'wb') as dest:
            copyfileobj(src, dest)


def read_language_data(files, dump_version):
    return [(line_count,
             lang_code,
             query_language(lang_code).canonical_name,
             '[https://tools-static.wmflabs.org/digero/%s/wanted/%s.jsonl.bz2 %s.jsonl.bz2]'
             % (dump_version, lang_code, lang_code),
             file) for (line_count, lang_code, file) in [(sum(1 for _ in open(f)),
                                                          split(dirname(f))[-1],
                                                          f) for f in files]]


def _load_config() -> dict:
    config_file = join(dirname(__file__), '..', '..', '..', '..', '..', 'wiktionary.toml')
    with open(config_file, 'rb') as f:
        return tomllib.load(f)


def main(wanted_entries_dir):
    if not exists(wanted_entries_dir):
        raise Exception('Directory %s not found' % wanted_entries_dir)

    config = _load_config()

    dump_version = config['dump']['version']
    wiktionary_wanted_page = config['wiktionary']['wanted_page']
    wiktionary_wanted_template = config['wiktionary']['wanted_template']
    www_static = expanduser(config['static_dir'])

    assert dump_version
    assert wiktionary_wanted_page

    files = glob('%s/*/*.json' % wanted_entries_dir)
    if len(files) == 0:
        raise Exception('no files to publish')

    language_data = read_language_data(files, dump_version)
    publish_overview_table(language_data, wiktionary_wanted_page, dump_version)

    for (_, lang_code, canonical_name, _, file) in language_data:
        publish_language(file,
                         canonical_name,
                         lang_code,
                         wiktionary_wanted_page,
                         wiktionary_wanted_template,
                         dump_version)

        if exists(www_static):
            archive_data(file, www_static, lang_code, dump_version)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Publish wanted entries.')
    parser.add_argument('wanted-entries', help='The wanted entries directory')
    args = vars(parser.parse_args())

    main(args['wanted-entries'])
