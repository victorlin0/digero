#!/usr/bin/env python
import typing

from mistletoe import Document
from mistletoe.block_token import List, Paragraph
from mistletoe.span_token import EscapeSequence, Link, RawText, SpanToken
from urllib.parse import urlparse, unquote
from typing import Optional


def link_target(text: str) -> Optional[tuple[str, str]]:
    match Document(text).children:
        case [Paragraph() as paragraph]:
            match paragraph.children:
                case [RawText(content=content)]:
                    return content, content
                case [Link() as link]:
                    components = urlparse(link.target)
                    target = unquote(components.path.replace('./', ''))
                    return _to_string(link.children), target
                case other:
                    return None
        case [List() as _list] if len(_list.children) == 1:  # type: ignore
            return text, text
        case other:
            raise Exception(f"Unexpected token: {other} ({text})")


@typing.no_type_check
def _to_string(nodes: list[SpanToken]) -> str:
    match nodes:
        case [RawText(content=content)]:
            return str(content)
        case [EscapeSequence(children=children)]:
            return _to_string(children)
        case [a, *rest]:
            return _to_string([a]) + _to_string(rest)
        case other:
            raise Exception(f"Unexpected token: {other}")
