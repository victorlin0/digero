#!/usr/bin/env python
from os import path, makedirs
from dataclasses import dataclass, field
from itertools import chain
from typing import Any, TypeAlias, Optional
from collections.abc import Iterator, Iterable
from .commonmark import link_target
from peewee import IntegrityError
from dataclasses_json import dataclass_json, DataClassJsonMixin
from ipanema import query_language
from ipanema.models import Language
from lexi.fixture import Database
from lexi.models import (Headword, Entry as LexiEntry, Etymology, Metadata,
                         Relation as LexiRelation, Sense as LexiSense,
                         Pronunciation as LexiPronunciation)
from lexi.models.jwktl import PartOfSpeech, PronunciationType

SCHEMA_VERSION = "202011010000"


@dataclass_json
@dataclass
class Relation:
    target: str
    type: int


@dataclass_json
@dataclass
class Sense:
    definition: str
    number: dict[str, int] = field(default_factory=dict)
    relations: list[Relation] = field(default_factory=list)


@dataclass_json
@dataclass
class Pronunciation:
    text: str
    qualifier: list[str] = field(default_factory=list)


@dataclass_json
@dataclass
class HeadwordLine:
    headword: str
    transliteration: Optional[str] = None
    gender: list[int] = field(default_factory=list)
    inflections: list[Any] = field(default_factory=list)

    def to_data(self) -> dict[str, Any]:
        data: dict[str, Any] = {
            'headword': self.headword
        }
        if transliteration := self.transliteration:
            data["transliterations"] = [transliteration]

        if len(self.gender) > 0:
            data["genders"] = self.gender
        return data


CommonMarkStr: TypeAlias = str


@dataclass_json
@dataclass
class Entry(DataClassJsonMixin):
    pageTitle: str
    headwordLines: list[HeadwordLine]
    partOfSpeech: str
    senses: list[Sense]
    categories: list[str]
    pronunciations: list[Pronunciation]
    etymology: CommonMarkStr | None = None
    usageNotes: CommonMarkStr | None = None

    def pos(self) -> PartOfSpeech | None:
        return PartOfSpeech.from_string(self.partOfSpeech)

    def is_translation_page(self) -> bool:
        return self.pageTitle.endswith('/translations')


def make_dictionary(output: str,
                    dump_version: str,
                    schema_version: str,
                    language: str,
                    entries: Iterable[Entry],
                    index: bool = False):
    output_dir = path.dirname(output)
    if not path.exists(output_dir):
        makedirs(output_dir)

    with Database(':memory:') as db:
        db.create_tables()

        for entry in entries:
            if not entry.is_translation_page():
                create_entry(entry)

        _link_relations()
        _insert_metadata(dump_version, schema_version, query_language(language))

        db.pragma('user_version', int(schema_version[:8]))
        if index:
            print("reindexing")
            db.reindex()
        db.vacuum()
        db.export(output)


def create_entry(entry: Entry) -> LexiEntry | None:
    pos = entry.pos()
    if pos is None:
        return None
    # assert pos is not None, f"unknown pos {entry.partOfSpeech} for {entry}"
    if len(entry.headwordLines) == 0:
        print(f"no headword lines in '{entry.pageTitle}'")
        return None

    headword = _make_headword(entry, pos)
    _insert_pronunciations(entry, headword)

    lexi_entry = LexiEntry.create(headword_text=entry.pageTitle,
                                  pos=pos,
                                  data=entry.headwordLines[0].to_data(),
                                  etymology=_insert_etymology(entry),
                                  usage_notes=entry.usageNotes,
                                  headword=headword)

    _insert_senses(lexi_entry, entry.senses)

    return lexi_entry


def _make_headword(entry, pos) -> Headword:
    return Headword.get_or_none(headword_text=entry.pageTitle) \
        or Headword.create(headword_text=entry.pageTitle, pos=pos)


def _insert_etymology(entry) -> Etymology | None:
    return Etymology.create(etymology_text=etymology) if (etymology := entry.etymology) else None


def _insert_pronunciations(entry: Entry, headword: Headword):
    for pronunciation in entry.pronunciations:
        if len(pronunciation.qualifier) == 0:
            LexiPronunciation.create(type=PronunciationType.IPA,
                                     pronunciation_text=pronunciation.text,
                                     headword=headword)
        else:
            for qualifier in pronunciation.qualifier:
                LexiPronunciation.create(type=PronunciationType.IPA,
                                         pronunciation_text=pronunciation.text,
                                         note=qualifier,
                                         headword=headword)


def _insert_senses(entry: LexiEntry, senses: list[Sense]):
    for sense in senses:
        lexi_sense = LexiSense.create(entry=entry,
                                      sense_text=sense.definition)
        _insert_relations(entry, lexi_sense, sense.relations)


def _insert_relations(entry: LexiEntry, lexi_sense: LexiSense, relations: list[Relation]):
    for relation in relations:
        try:
            LexiRelation.create(type=relation.type,
                                entry=entry,
                                sense=lexi_sense,
                                target_headword_text=relation.target)
        except IntegrityError:
            print(f"ignoring duplicate relation {relation} in entry {entry.headword_text}")


def _fetch_relations(batch_size=200) -> Iterator[list[LexiRelation]]:
    page = 1
    while True:
        relations = LexiRelation.select().paginate(page, paginate_by=batch_size)
        if len(relations) == 0:
            break
        else:
            page += 1
            yield list(relations)


def _link_relations():
    for batch in _fetch_relations():
        for relation in batch:
            match link_target(relation.target_headword_text):
                case (title, target):
                    if headword := Headword.get_or_none(headword_text=target):
                        relation.target_headword = headword
                        relation.target_headword_text = title
                case None: pass

        LexiRelation.bulk_update(batch, fields=[LexiRelation.target_headword,
                                                LexiRelation.target_headword_text])


def _insert_metadata(dump_version: str, schema_version: str, language: Language):
    metadata = {
        Metadata.DB_SCHEMA_KEY: schema_version,
        Metadata.LANGUAGE: language.code,
        Metadata.LANGUAGE_NAME: language.canonical_name,
        Metadata.DUMP_VERSION: dump_version,
        Metadata.HEADWORDS: Headword.select().count(),
        Metadata.TRANSLATIONS: 0
    }
    for (key, value) in metadata.items():
        Metadata.create(key=key, value=value)


def _load_data(json_path: str) -> Iterator[Entry]:
    with open(json_path, 'r') as fileinput:
        print(f"reading {json_path}")
        for line in fileinput:
            yield Entry.from_json(line)


def _load_entries(paths: list[str]) -> Iterator[Entry]:
    return chain.from_iterable(_load_data(p) for p in paths)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Create dictionary')
    parser.add_argument('content.ndjson', help='The dictionary content', nargs='*')
    parser.add_argument('--language', help='The language code', required=True)
    parser.add_argument('--dump-version', help='The dump version', required=True)
    parser.add_argument('--index', help='Index the content', action='store_true')
    parser.add_argument('--out', help='The database output', required=True)

    args = vars(parser.parse_args())
    make_dictionary(args['out'],
                    dump_version=args['dump_version'],
                    schema_version=SCHEMA_VERSION,
                    language=args['language'],
                    entries=_load_entries(args['content.ndjson']),
                    index=args['index'])
