import base64
import os.path
import re

from typing import Any
from mypy_boto3_s3 import S3Client
import boto3
import hashlib


def upload(dictionaries: list[str], bucket: str, region: str):
    dict_regex = re.compile(r"wiktionary-(\d+)-(\w+).sqlite")
    for dictionary in dictionaries:
        basename = os.path.basename(dictionary)

        if match := dict_regex.match(basename):
            date = match.group(1)
            key = f"db/{date}/{basename}"
            upload_dictionary(dictionary, bucket, key, region)
        else:
            raise Exception(f"Did not match {basename}")


def calculate_sha256(path) -> Any:
    with open(path, 'rb') as file:
        digest = hashlib.file_digest(file, 'sha256')
    return digest


def upload_dictionary(dictionary: str, bucket: str, key: str, region: str):
    client: S3Client = boto3.client('s3', region)
    checksum_sha256 = calculate_sha256(dictionary)
    base64_sha256 = str(base64.b64encode(checksum_sha256.digest()),
                        encoding='utf-8')

    with open(dictionary, 'rb') as file:
        print(f"uploading to {bucket}:{key}")

        client.put_object(Bucket=bucket,
                          Body=file,
                          Key=key,
                          ContentType='application/x-sqlite3',
                          ChecksumAlgorithm='SHA256',
                          ChecksumSHA256=base64_sha256,
                          Metadata={
                              'sha256': checksum_sha256.hexdigest()
                          })


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Publish dictionary')
    parser.add_argument('wiktionary-20230701-XX.sqlite',
                        help='The dictionaries', nargs='*')
    parser.add_argument("--bucket", help='S3 target bucket', required=True)
    parser.add_argument("--region", help='S3 region', default='eu-central-1')

    args = vars(parser.parse_args())
    upload(args['wiktionary-20230701-XX.sqlite'],
           bucket=args['bucket'],
           region=args['region'])
