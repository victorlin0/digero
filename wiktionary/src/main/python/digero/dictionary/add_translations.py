#!/usr/bin/env python
from dataclasses import dataclass, field
from itertools import chain
from collections.abc import Iterator, Iterable
from dataclasses_json import dataclass_json, DataClassJsonMixin, config as json_config
from lexi.fixture import Database
from lexi.models import (Headword as LexiHeadword,
                         Translation as LexiTranslation, Metadata)
from lexi.models.jwktl import PartOfSpeech


@dataclass_json
@dataclass
class SerializedTranslation:
    text: str
    language: str
    script: str | None


@dataclass_json
@dataclass
class SerializedTranslationGroup:
    gloss: str
    translations: list[SerializedTranslation]


@dataclass_json
@dataclass
class TranslationEntry(DataClassJsonMixin):
    headword: str = field(metadata=json_config(field_name="_1"))
    pos: str = field(metadata=json_config(field_name="_2"))
    groups: list[SerializedTranslationGroup] = field(metadata=json_config(field_name="_3"))


def _load_data(path: str) -> Iterator[TranslationEntry]:
    with open(path, 'r') as fileinput:
        print(f"reading {path}")
        for line in fileinput:
            yield TranslationEntry.from_json(line)


def _load_translations(paths: list[str]) -> Iterator[TranslationEntry]:
    return chain.from_iterable(_load_data(path) for path in paths)


def add_translations(db_path: str, translations: Iterable[TranslationEntry], reindex: bool):
    with Database(db_path) as db:
        LexiTranslation.delete().execute()

        for entry in translations:
            for group in entry.groups:
                for translation in group.translations:
                    # print(entry.headword, translation)
                    if headword := LexiHeadword.get_or_none(LexiHeadword.headword_text == translation.text):
                        LexiTranslation.create(translation_text=entry.headword,
                                               pos=PartOfSpeech.from_string(entry.pos),
                                               headword=headword,
                                               headword_text=translation.text,
                                               sense=group.gloss)

        if reindex:
            print("reindexing")
            db.reindex()

        Metadata.update(value=LexiTranslation.select().count()) \
                .where(Metadata.key == Metadata.TRANSLATIONS) \
                .execute()

        db.vacuum()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Add translations')
    parser.add_argument('translation.ndjson', help='The translations', nargs='*')
    parser.add_argument('--db', help='The database input', required=True)
    parser.add_argument('--index', help='Index the content', action='store_true')

    args = vars(parser.parse_args())
    add_translations(args['db'], _load_translations(args['translation.ndjson']), args['index'])
