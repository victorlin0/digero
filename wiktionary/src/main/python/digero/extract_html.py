#!/usr/bin/env python
import sys

from fastparquet import ParquetFile


def dump_entry(language: str, entry: str, dump: str):
    pf = ParquetFile(dump)

    filters = [("language", "=", language),
               ("entry", "=", entry)]

    df = pf.to_pandas(filters=filters,
                      row_filter=False,
                      index=["entry"])

    print(df.loc[entry]['html'])


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Extract from HTML dump')
    parser.add_argument('dump-html.parquet', help='parsed HTML dump')
    parser.add_argument('--language', help='language', default='en')
    parser.add_argument('[lang:]entry', help='entry')

    args = vars(parser.parse_args())
    entry = args['[lang:]entry']
    language = args['language']
    if ':' in entry:
        language, entry = entry.split(':', 2)

    try:
        dump_entry(language, entry, args['dump-html.parquet'])
    except KeyError:
        print("Entry not found", file=sys.stderr)
        exit(1)
