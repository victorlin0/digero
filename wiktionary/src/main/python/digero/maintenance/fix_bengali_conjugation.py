#!/usr/bin/env python3
# encoding: utf-8

import pywikibot
from pywikibot.diff import PatchManager
import mwparserfromhell

# Fixes nesting in the form of:
#
# ====Chalita Conjugation====
# {{bn-conj-ano|জাগ|jag}}
#
# ====Sadhu Conjugation====
# {{bn-sadhu-conj-ano|জাগ|jag}}
#
# etc. converting it to:
# ====Conjugation====
# ; Chalita
# ; Sadhu
if __name__ == '__main__':
    pages: list[str] = []
    site = pywikibot.Site()

    for page_name in pages:
        page = pywikibot.Page(site, page_name)
        print(page)
        text = page.text
        parsed = mwparserfromhell.parse(text)
        sections = parsed.get_sections(matches=r"(Chalita|Sadhu) Conjugation")

        if len(sections) != 2:
            print(f"skipping ${page}")
            continue

        if sections[0].nodes[0].level != 4:
            print(f"skipping level ${page}")
            continue

        content = {str(section.nodes[0].title): str(section.nodes[2]) for section in sections}
        parsed.remove(sections[1])
        parsed.replace(sections[0], """====Conjugation====
; Chalita
%s
; Sadhu
%s
""" % (content['Chalita Conjugation'], content['Sadhu Conjugation']))
        replaced = str(parsed)

        if text != replaced:
            page.text = replaced
            PatchManager(text, replaced, context=3).print_hunks()
            page.save(summary='Normalized section header', watch='nochange')
