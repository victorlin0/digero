package digero.wiktionary.parser

import java.util.regex.Pattern

type NGramCounts = Map[String, Int]


object Similarity:
  private val NGRAM_PATTERN = Pattern.compile("^_?[^0-9?!\\-_/]*_?$")
  private val WORD_PATTERN = Pattern.compile("\\w+")

  /**
   * Adapted from JWKTL.
   * [[https://github.com/dkpro/dkpro-jwktl/blob/ce96b0184efd89dc8e941d9afc9f7e3cb11ac30c/src/main/java/de/tudarmstadt/ukp/jwktl/parser/en/components/ENSemanticRelationHandler.java#L60 ENSemanticRelationHandler.java]]
   */
  def findMostSimilar(text: String, candidates: Seq[String]): Option[String] =
    var best1GramScore = -1.0
    var best3GramScore = -1.0
    val textUnigrams = computeUnigramCounts(text)
    val textTrigrams = computeTrigramCounts(text)
    var best1GramMatch: Option[String] = None
    var best3GramMatch: Option[String] = None

    for
      candidate <- candidates
    do
      val similarity1Gram = similarity(textUnigrams, computeUnigramCounts(candidate))
      if similarity1Gram > best1GramScore then
        best1GramScore = similarity1Gram
        best1GramMatch = Some(candidate)

      val similarity3Gram = similarity(textTrigrams, computeTrigramCounts(candidate))
      if similarity3Gram > best3GramScore then
        best3GramScore = similarity3Gram
        best3GramMatch = Some(candidate)

    if best1GramScore <= 0 && best3GramScore <= 0 then
      None
    else if best1GramScore > best3GramScore then
      best1GramMatch
    else
      best3GramMatch

  /**
   * Adapted from JWKTL's
   * [[https://github.com/dkpro/dkpro-jwktl/blob/ce96b0184efd89dc8e941d9afc9f7e3cb11ac30c/src/main/java/de/tudarmstadt/ukp/jwktl/parser/util/SimilarityUtils.java#L102 SimilarityUtils.java]].
   * Uses the [[https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient Sørensen–Dice coefficient]]
   * to calculate the similarity.
   *
   * @return a similarity value from 0 (completely different) to 1 (exactly the same)
   */
  def similarity(ngramsA: NGramCounts, ngramsB: NGramCounts): Double =
    val common = ngramsA.common(ngramsB)
    if common.isEmpty then
      return 0

    val combined = ngramsA.combine(ngramsB)
    val all = combined.values.sum
    val a = ngramsA.normalize(all)
    val b = ngramsB.normalize(all)

    2 * common.normalize(all) / (a + b)

  /**
   * Adapted from JWKTL.
   */
  def computeUnigramCounts(text: String): NGramCounts =
    val ngrams = collection.mutable.Map[String, Int]()
    val matcher = WORD_PATTERN.matcher(text)
    while matcher.find() do
      val word = matcher.group().toLowerCase()
      ngrams.updateWith(word)(value => value.map(_ + 1).orElse(Some(1)))
    ngrams.toMap


  def computeTrigramCounts(text: String): NGramCounts = computeNGramCounts(text, 3 to 3)

  /**
   * Adapted from JWKTL.
   * [[https://github.com/dkpro/dkpro-jwktl/blob/ce96b0184efd89dc8e941d9afc9f7e3cb11ac30c/src/main/java/de/tudarmstadt/ukp/jwktl/parser/util/SimilarityUtils.java#L43 SimilarityUtils.java]]
   */
  private def computeNGramCounts(text: String, range: Range): NGramCounts =
    val tokens = text.split("\\s").map(_.mkString("_", "",  "_"))
    val ngrams = collection.mutable.Map[String, Int]()

    for order <- range do
      for token <- tokens do
        for
          i <- 0 to token.length - order
          ngram = token.substring(i, i + order)
          if NGRAM_PATTERN.matcher(ngram).find()
        do
          ngrams.updateWith(ngram)(value => value.map(_ + 1).orElse(Some(1)))

    ngrams.updateWith("_")(value => value.map(_ / 2))
    ngrams.toMap


  extension (ngramCounts: NGramCounts)
    private def combine(other: NGramCounts): NGramCounts =
      val combined = collection.mutable.Map[String, Int]()
      combined.addAll(other)
      for (k, v) <- ngramCounts do
        combined.updateWith(k)(value => value.map(Math.max(_, v)).orElse(Some(v)))
      combined.toMap

    private def common(other: NGramCounts): NGramCounts =
      (for
        key <- ngramCounts.keySet.intersect(other.keySet)
        valA <- ngramCounts.get(key)
        valB <- other.get(key)
      yield
        (key, Math.min(valA, valB))
      ).toMap

    private def normalize(all: Double): Double =
      ngramCounts.values.foldLeft(0d)((m, value) => m + Math.log(value / all))
