package digero.wiktionary.parser

import digero.wiktionary.model.Sense
import digero.wiktionary.parser.JsoupExtensions.*
import org.jsoup.nodes.{Element, Node}
import org.jsoup.select.NodeFilter.FilterResult

class SenseParser extends HTMLParser with Logger:
  private lazy val nymParser = NymParser()

  def parse(html: String)(using entryContext: EntryContext): Seq[Sense] =
    val document = parseDocument(html)
    val lists = document.select("body > ol")

    lists.size() match
      case 0 => Seq.empty
      case 1 => parseList(lists.first())
      case _ =>
        log.warn(s"more than one list found in entry ${entryContext.entry}")
        Seq.empty

  private def parseList(element: Element)(using entryContext: EntryContext): Seq[Sense] =
    for
      li <- element.select("> li")
      element = li.clone().filter((node: Node, _) =>
        node match
            case e: Element if Set("dl", "ul", "ol").contains(e.tagName) => FilterResult.REMOVE
            case _ => FilterResult.CONTINUE
      )
      html = element.preprocessed
      text = element.text() if !text.strip().isBlank

      kind = CSSClass.applicable(li).flatMap(_.senseKind)
      nyms = li.select("> dl dd span.nyms")

      subList = li.select("> ol").headOption
      subsenses = subList.map(parseList).getOrElse(Seq.empty)
    yield Sense(
      text,
      html,
      subsenses=subsenses,
      kind=kind,
      nyms=nyms.flatMap(nymParser.parse))
