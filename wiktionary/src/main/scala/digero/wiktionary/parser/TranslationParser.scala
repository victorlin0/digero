package digero.wiktionary.parser

import digero.wiktionary.model.serialization.TextConverter
import digero.wiktionary.model.{Translation, TranslationGroup, TranslationSubpage}
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.TranslationParser.IGNORED_LANG_CLASSES
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import scala.jdk.CollectionConverters.*

class TranslationParser(using textConverter: TextConverter)
                       (using context: EntryContext) extends HTMLParser with Logger:

  def parseTranslationSection(html: String): Seq[TranslationGroup] =
    if context.languageCode != "en" then
      log.warn(s"translation found in entry $context")

    val document = parseDocument(html)
    val translationTables = document.select("table.translations")

    parseSubpages(document) ++ parseTables(translationTables)


  private def parseTables(translationTables: Elements): Seq[TranslationGroup] =
    for
      translationTable <- translationTables
      gloss = stripMarkup(translationTable.attr("data-gloss"))
              if !gloss.startsWith("Translations to be checked")
    yield
      TranslationGroup(gloss = gloss, translations = parseTranslations(gloss, translationTable))

  private def parseSubpages(document: Element): Seq[TranslationGroup] =
    for
      element <- document.select("div.pseudo.NavFrame")
      template <- element.templates if template.name == "see translation subpage"

      section = template.headParameter
      pageName = template.tailParameter

      subpage = TranslationSubpage(pageName.getOrElse(context.entry.name), section)
    yield
      TranslationGroup(gloss="",
        translations=Seq.empty,
        subpage=Some(subpage)
      )

  private def parseTranslations(gloss: String, table: Element): Seq[Translation] =
    for
      listItem <- table.select("td > ul > li")
      translation <- parseListItem(gloss, listItem)
    yield
      translation

  private def parseListItem(gloss: String, listItem: Element): Seq[Translation] =
    for
      // requested translation have class="trreq" data-lang="xxx", not lang="xxx"
      spanLang <- listItem.select("span[lang]") if shouldInclude(spanLang)
      language = spanLang.attr("lang")
      translation <- parseTranslationContent(gloss, language, spanLang)
      script = spanLang.className()
      transliteration = spanLang
                          .nextElementSiblings()
                          .select(s"span[lang=$language-Latn]")
                          .headOption.map(_.text())
      qualifier = spanLang
                    .nextElementSiblings()
                    .select("span.qualifier-content")
                    .headOption.map(_.text())
    yield
      Translation(
          language,
          translation,
          transliteration,
          script,
          qualifier,
          needsVerifying = false
      )

  private inline def shouldInclude(span: Element): Boolean =
    val checks = Seq[Element => Boolean](isEnglish, isIgnored, isChineseSeparator, isMistaggedJapanese)
    !checks.exists(_.apply(span))

  private def isEnglish(span: Element): Boolean = span.attr("lang") == "en"
  private def isIgnored(span: Element): Boolean = IGNORED_LANG_CLASSES.exists(span.hasClass)
  private def isChineseSeparator(span: Element): Boolean = span.attr("lang") == "zh" && span.text() == "／"
  private def isMistaggedJapanese(span: Element): Boolean = span.attr("lang") == "ja" && span.className() == "Latn"
  
  private def stripMarkup(s: String): String =
    s.replace("[[", "")
     .replace("]]", "")
     .replace("'''", "")
     .replace("''", "")


  /**
   * Where no idiomatic translation exists, a translation may be given as separate, square bracketed links inside
   * the {{t}} templates. For example, in the verb star, which has no literal equivalent in French:
   * French: {{t|fr|[[tenir]] le [[rôle]] [[principal]]}}
   */
  private def parseTranslationContent(gloss: String, language: String, element: Element): Option[String] =
    element.select("a[rel=mw:WikiLink]").asScala.toSeq match
      // if there's just a single translation, use that
      case singleLink :: Nil => Some(singleLink.text())
      // otherwise, convert individual links to CommonMark
      case head :: tail => textConverter(element.html())
      case Nil =>
        log.warn(s"empty translations in ${context.entry}#$gloss for $language")
        None


object TranslationParser:
  private val IGNORED_LANG_CLASSES = Set("attentionseeking", "tr")