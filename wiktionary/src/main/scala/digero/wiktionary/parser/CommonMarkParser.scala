package digero.wiktionary.parser;

import com.vladsch.flexmark.ast.{Link, Text}
import com.vladsch.flexmark.formatter.Formatter
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.ast.{Document, Node, NodeVisitor, TextCollectingVisitor, VisitHandler, Visitor}
import com.vladsch.flexmark.util.data.MutableDataSet

import scala.annotation.unused


class CommonMarkParser:
  private val parser = Parser.builder().build()

  def convertToPlain(commonMark: String): String =
    val textCollectingVisitor = new TextCollectingVisitor()
    val document = parser.parse(commonMark)
    val text = textCollectingVisitor.collectAndGetText(document)
    text

  def simplify(commonMark: String, language: String): String =
    val document = parser.parse(commonMark)

    val visitor = LinkVisitor(language.replace(" ", "_"))
    visitor.visit(document)

    if visitor.valid then
      render(document)
    else
      commonMark

  private def render(document: Document): String =
    val formatOptions = MutableDataSet()
    val formatter = Formatter.builder(formatOptions).build
    formatter.render(document).strip()


private class LinkVisitor(val language: String):
  private val visitor = NodeVisitor(
    VisitHandler(classOf[Link], visitLink _),
    VisitHandler(classOf[Text], visitText _)
  )

  var valid: Boolean = true

  def visit(node: Node): Unit = visitor.visit(node)

  private def visitText(@unused text: Text): Unit = valid = false

  private def visitLink(link: Link): Unit =
    if link.getAnchorRef.unescape() == language && link.getTitle == link.getText then
      replaceWithText(link)
    else
      valid = false

  private def replaceWithText(link: Link): Unit =
    link.getParent.appendChild(new Text(link.getText.unescape()))
    link.unlink()
