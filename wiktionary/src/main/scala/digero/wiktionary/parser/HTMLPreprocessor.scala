package digero.wiktionary.parser

import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element, Node, TextNode}
import digero.wiktionary.parser.JsoupExtensions.*

object HTMLPreprocessor:

  def preprocess(html: String): Document =
    val document = Jsoup.parseBodyFragment(html)
    removeUnwanted(document)
    document

  def removeUnwanted(element: Element): Unit =
    removeUnwantedElements(element)
    removeUnwantedAttributes(element)

  private def removeUnwantedElements(document: Element): Unit =
    for
      selector <- FILTER_ELEMENTS
      element <- document.select(selector)
    do
      removeIfBlankText(element.previousSibling())
      removeIfBlankText(element.nextSibling())

      element.remove()

  private def removeUnwantedAttributes(document: Element): Unit =
    for
      attribute <- FILTER_ATTRIBUTES
      element <- document.getElementsByAttribute(attribute)
    do
      element.removeAttr(attribute)

  private def removeIfBlankText(node: Node): Unit =
    node match
      case text: TextNode if text.text().isBlank => text.remove()
      case _ =>

  private val FILTER_ATTRIBUTES = Array(
    "about",
    "data-mw",
    "id"
  )

  private val FILTER_ELEMENTS = Array(
    ".maintenance-line,.maintenance-box", // Template:maintenance_line
    ".request-box",                       // Template:request box
    ".number-box",                        // Template:number box
    ".PIE-word",                          // Template:PIE word
    ".sister-project",                    // Template:wikipedia etc.
    ".attentionseeking",                  // Template:attention
    ".color-panel",                       // Template:color_panel
    ".audiotable",                        // Template:audio
    "sup.mw-ref",                         // Reference link
    "div.mw-references-wrap",             // Reference list
    "abbr",                               // gets mangled by html2text
    "figure",                             // no images (LEXI-71)
    ".thumb.tmulti",                      // Template:multiple images
    "link[rel=\"mw:PageProp/Category\"]", // categories
    "body > h3, h4, h5, h6",              // first heading
    "table",                              // workaround for tables w/o specific CSS classes
    ".mw-empty-elt"                       // empty element
  )
