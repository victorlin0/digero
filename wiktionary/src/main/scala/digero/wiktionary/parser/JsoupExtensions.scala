package digero.wiktionary.parser

import digero.model.{Template, TemplateParameter, TemplateParameterMap}
import digero.wiktionary.model.parsoid.{DataTemplateParams, ParsoidAttribs, ParsoidCaption, ParsoidData, ParsoidErrors, ParsoidTemplate, ParsoidTemplateStyle}
import org.jsoup.nodes.Document.OutputSettings
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements

import scala.collection.WithFilter
import scala.jdk.CollectionConverters.*
import scala.util.Try

object JsoupExtensions:
  extension (elements: Elements)
    def foreach[U](f: Element => U): Unit = elements.asScala.foreach(f)
    def flatMap[B](f: Element => IterableOnce[B]): Seq[B] = elements.asScala.toSeq.flatMap(f)
    def map[B](f: Element => B): Seq[B] = elements.asScala.toSeq.map(f)
    def withFilter(f: Element => Boolean): WithFilter[Element, Seq] = elements.asScala.toSeq.withFilter(f)

    def head: Element = elements.first()

    def headOption: Option[Element] =
      if elements.size() == 0 then None else Some(elements.get(0))

  extension (element: Element)
    def head: Element = element.child(0)
    def headOption: Option[Element] = if element.childrenSize() == 0 then None else Some(element.child(0))
    def attrOpt(key: String): Option[String] = if element.hasAttr(key) then Some(element.attr(key)) else None
    def classes: Seq[String] = element.classNames().iterator().asScala.toSeq

    def parsoidData: Option[ParsoidData] =
        attrOpt("data-mw").flatMap { data =>
          Try(ParsoidTemplate.fromJSON(data))
            .orElse(Try(ParsoidCaption.fromJSON(data)))
            .orElse(Try(ParsoidTemplateStyle.fromJSON(data)))
            .orElse(Try(ParsoidAttribs.fromJSON(data)))
            .orElse(Try(ParsoidErrors.fromJSON(data)))
            .toOption
        }

    def templates: Seq[Template] =
      parsoidData match
        case Some(ParsoidTemplate(parts)) =>
          for
            part <- parts
            container <- part.toOption
            template = container.template
            name <- template.name

          yield Template(name, template.params.toTemplateParamMap)
        case _ => Seq.empty


    def preprocessed: String =
      val copy = element.clone()
      HTMLPreprocessor.removeUnwanted(copy)
      copy.html()

  extension (document: Document)
    def serialize: String =
      document.outputSettings.prettyPrint(false)
      document.outputSettings.syntax(OutputSettings.Syntax.xml)
      document.body.html

  extension (dataTemplateParams: DataTemplateParams)
    private def toTemplateParamMap: TemplateParameterMap =

      val values: Seq[(TemplateParameter, String)] =
        for
          (key, value) <- dataTemplateParams.toSeq
        yield
          key.toIntOption match
            case Some(paramIndex) => (Left(paramIndex), value.wt)
            case None             => (Right(key), value.wt)

      TemplateParameterMap(values)