package digero.wiktionary.parser

import com.vladsch.flexmark.formatter.Formatter
import com.vladsch.flexmark.html.renderer.ResolvedLink
import com.vladsch.flexmark.html2md.converter.{FlexmarkHtmlConverter, HtmlLinkResolver, HtmlLinkResolverFactory, ExtensionConversion, HtmlNodeConverterContext}
import com.vladsch.flexmark.parser.{Parser, ParserEmulationProfile}
import com.vladsch.flexmark.util.data.MutableDataSet
import digero.wiktionary.parser.CommonMarkConverter.specialChars
import digero.wiktionary.parser.JsoupExtensions.*
import org.jsoup.nodes.Node

import scala.jdk.CollectionConverters.*

class CommonMarkConverter:

  def convert(html: String): Option[String] =
    val preprocessed = HTMLPreprocessor.preprocess(html)
    if preprocessed.text().isBlank then
      return None

    val markdown = flexmarkConverter.convert(preprocessed.serialize, 0)
    val document = parser.parse(markdown)
    Some(formatter.render(document).stripTrailing())

  private lazy val flexmarkConverter: FlexmarkHtmlConverter =
    FlexmarkHtmlConverter.builder(converterOptions).linkResolverFactory(LinkResolverFactory()).build()

  private lazy val parser: Parser = Parser.builder(parserOptions).build()
  private lazy val formatter: Formatter = Formatter.builder(formatterOptions).build()

  private lazy val formatterOptions: MutableDataSet = MutableDataSet()
    .set(Formatter.FORMATTER_EMULATION_PROFILE, emulationProfile)
    .set(Formatter.MAX_TRAILING_BLANK_LINES, 0)

  private lazy val emulationProfile = ParserEmulationProfile.COMMONMARK_0_29
  private lazy val parserOptions: MutableDataSet = MutableDataSet()

  private lazy val converterOptions: MutableDataSet = MutableDataSet()
    .set(FlexmarkHtmlConverter.OUTPUT_ATTRIBUTES_ID, false)
    .set(FlexmarkHtmlConverter.EXT_INLINE_SUP, ExtensionConversion.NONE)
    .set(FlexmarkHtmlConverter.TYPOGRAPHIC_REPLACEMENT_MAP, specialChars.asJava)
//    .set(FlexmarkHtmlConverter.DUMP_HTML_TREE, true)
    .set(FlexmarkHtmlConverter.EXT_INLINE_INS, ExtensionConversion.TEXT)

object CommonMarkConverter:
    val specialChars:Map[String, String] = Map(
      "&ldquo;" -> "“",
      "&rdquo;" -> "”",
      "&lsquo;" -> "‘",
      "&rsquo;"-> "’",
      "&apos;" -> "'",
      "&laquo;" -> "«",
      "&raquo;" -> "»",
      "&hellip;" -> "…",
      "&endash;" -> "–",
      "&emdash;" -> "—"
    )


private class LinkResolverFactory extends HtmlLinkResolverFactory:
  override def getAfterDependents: java.util.Set[Class[_]] = null
  override def getBeforeDependents: java.util.Set[Class[_]] = null
  override def affectsGlobalScope() = false
  override def apply(context: HtmlNodeConverterContext): HtmlLinkResolver = customLinkResolver

object customLinkResolver extends HtmlLinkResolver:
  override def resolveLink(node: Node,
                           context: HtmlNodeConverterContext,
                           link: ResolvedLink): ResolvedLink =
    import sttp.model.Uri

    if link.getUrl.startsWith("./") then
      val uri = Uri.unsafeParse(link.getUrl)
      if uri.params.get("action").isDefined then
        // remove all query parameters
        link.withUrl(uri.withParams(Map.empty).toString)
      else
        link
    else
      link