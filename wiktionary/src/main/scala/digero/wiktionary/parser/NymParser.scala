package digero.wiktionary.parser

import digero.wiktionary.model.lexical.{Nym, RelationType}
import org.jsoup.nodes.Element
import digero.wiktionary.parser.JsoupExtensions.*

// Parses a single "nym" line
class NymParser extends HTMLParser:
  private val commonMarkConverter = CommonMarkConverter()
  private val commonMarkParser = CommonMarkParser()
  
  def parse(element: Element)(using context: EntryContext): Seq[Nym] =
    require(element.classes.size >= 2)
    require(element.classes.head == "nyms")

    val nymType = element.classes(1)
    val relationType = RelationType(nymType)

    for
      link <- element.select(s"> span[lang=${context.languageCode}]")
      cmark <- commonMarkConverter.convert(link.html())
      simplified = commonMarkParser.simplify(cmark, context.languageName)
    yield Nym(relationType, context.entry.name, simplified)



