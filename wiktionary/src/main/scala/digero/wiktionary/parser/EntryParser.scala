package digero.wiktionary.parser

import digero.wiktionary.model.{EntryName, Level, ParsedEntry, Section, SectionHeading}
import org.jsoup.nodes.{Element, Node}
import digero.wiktionary.parser.JsoupExtensions.*
import org.jsoup.select.NodeFilter.FilterResult

class EntryParser extends HTMLParser with Serializable:
  def parse(entryName: EntryName, html: String): ParsedEntry[String] =
    val document = parseDocument(html)
    ParsedEntry(name = entryName, sections(document.select("body").first()))

  private def sections(element: Element): Seq[Section[String]] =
    for
      sectionElement <- element.select("> section")
      headerElement <- sectionElement.headOption
      name = headerElement.text()
      level <- Level(headerElement.tagName)
      (header, index) = Section.parse(name)
      content = sectionElement.clone().filter((node: Node, depth: Int) =>
          node match
            case e: Element if e.tagName == "section" && depth > 0 => FilterResult.REMOVE
            case _ => FilterResult.CONTINUE
      ).html()
      childSections = sections(sectionElement)
    yield
      Section(name, level, header, index, Some(content), childSections)