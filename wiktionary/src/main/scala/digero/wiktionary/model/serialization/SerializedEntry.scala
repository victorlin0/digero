package digero.wiktionary.model.serialization

import digero.wiktionary.model.lexical.Pronunciation
import digero.wiktionary.model.lexical.PronunciationType.IPA
import digero.wiktionary.model.{HeadwordLine, PartOfSpeechSection, TranslationGroup}

type TextConverter = String => Option[String]

case class SerializedEntry(
    pageTitle: String,
    headwordLines: Seq[SerializedHeadwordLine],
    partOfSpeech: String,
    senses: Seq[SerializedSense],
    etymology: Option[String],
    pronunciations: Seq[SerializedPronunciation],
    translations: Seq[SerializedTranslationGroup],
    usageNotes: Option[String],
    categories: Seq[String]
) derives CustomPickler.ReadWriter

case class Inflection(
  `type`: String,
  forms: Seq[String]
) derives CustomPickler.ReadWriter

case class SerializedTranslation(
  text: String,
  language: String,
  script: Option[String] // only set when non-Latin
) derives CustomPickler.ReadWriter

case class SerializedTranslationGroup(
  gloss: String,
  translations: Seq[SerializedTranslation],
  subpage: Option[String]
) derives CustomPickler.ReadWriter

object SerializedTranslationGroup:
  def apply(translationGroup: TranslationGroup): SerializedTranslationGroup =
    SerializedTranslationGroup(
      gloss=translationGroup.gloss,
      translations=translationGroup.translations.map(t => SerializedTranslation(t.translation, t.language, t.nonLatinScript)),
      subpage=translationGroup.subpage.map(_.toString)
    )

case class SerializedHeadwordLine(
  headword: String,
  transliteration: Option[String],
  inflections: Seq[Inflection],
  gender: Seq[Long]
) derives CustomPickler.ReadWriter

case class SerializedPronunciation(
  text: String,
  qualifier: Seq[String]
) derives CustomPickler.ReadWriter

object SerializedPronunciation:
  def apply(pronunciation: Pronunciation): SerializedPronunciation =
    SerializedPronunciation(pronunciation.text, pronunciation.qualifier)

object SerializedHeadwordLine:
  def apply(headwordLine: HeadwordLine): SerializedHeadwordLine =

    SerializedHeadwordLine(
      headwordLine.headword,
      transliteration = headwordLine.text.toOption.flatMap(_.transliterations.headOption),
      inflections = headwordLine.inflections.map(inflection => Inflection(inflection.`type`.toString, inflection.forms)),
      gender = headwordLine.genders.map(_.id)
    )

object SerializedEntry extends FromJSON[SerializedEntry]:
  def apply(pageTitle: String,
            etymology: Option[String],
            pronunciations: Seq[Pronunciation],
            partOfSpeech: PartOfSpeechSection)(using textConverter: TextConverter): SerializedEntry =

    SerializedEntry(
      pageTitle,
      headwordLines = partOfSpeech.headwordLines.map(SerializedHeadwordLine(_)),
      partOfSpeech = partOfSpeech.section.name,
      senses = partOfSpeech.senses.serialized,
      etymology = etymology.flatMap(textConverter),
      pronunciations = pronunciations.filter(_.`type` == IPA).map(SerializedPronunciation.apply),
      usageNotes = partOfSpeech.usageNotes.flatMap(textConverter),
      translations = partOfSpeech.translations.map(SerializedTranslationGroup.apply),
      categories = partOfSpeech.categories
    )

