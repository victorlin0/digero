package digero.wiktionary.model.serialization

import digero.wiktionary.model.Sense
import digero.wiktionary.model.lexical.Nym

/*
1
 1.1
  1.1.1
 1.2
2
 2.2
*/
type SenseNumber = (Long, Long, Long, Long)

extension (senseNumber: SenseNumber)
  def set(level: Int, value: Long): SenseNumber =
    level match
      case 0 => senseNumber.copy(_1=value)
      case 1 => senseNumber.copy(_2=value)
      case 2 => senseNumber.copy(_3=value)
      case 3 => senseNumber.copy(_4=value)
      case _ => assert(false)


case class SerializedRelation(
  target: String,
  `type`: Int
) derives CustomPickler.ReadWriter

object SerializedRelation:
  def apply(nym: Nym): SerializedRelation = SerializedRelation(nym.target, nym.`type`.ordinal)

case class SerializedSense(
   number: SenseNumber,
   definition: String,
   relations: Seq[SerializedRelation],
   // this references a gloss in the entries, translations
   translationGloss: Option[String]
) derives CustomPickler.ReadWriter

extension (senses: Seq[Sense])
  def serialized(using textConverter: TextConverter): Seq[SerializedSense] =
    def recurse(senses: Seq[Sense],
                start: SenseNumber,
                level: Int): Seq[SerializedSense] =

      senses.zipWithIndex.flatMap((sense, index) => {
        val number = start.set(level, index + 1)
        SerializedSense(number,
                        definition = textConverter(sense.html).getOrElse(""),
                        sense.nyms.map(SerializedRelation.apply),
                        None) +:
          recurse(sense.subsenses, number, level + 1)
      })

    recurse(senses, (0, 0, 0, 0), 0)

