package digero.wiktionary.model

import digero.wiktionary.model.lexical.Pronunciation
import digero.wiktionary.model.serialization.TextConverter
import digero.wiktionary.parser.{EntryContext, HeadwordLineParser, PronunciationParser, SenseParser, TranslationParser}

case class Entry[Content](
     name: EntryName,
     etymologySection: Option[Section[Content]] = None,
     posSections: Seq[Section[Content]] = Seq.empty[Section[Content]],
     remainingSections: Seq[Section[Content]] = Seq.empty[Section[Content]]
):
  def pageTitle: String = name.name

object Entry:
  def apply[T](parsedEntry: ParsedEntry[T]): Entry[T] =
    val (etymologies, posSections, rest) = parsedEntry.sections.split(_.isEtymology)
    assert(etymologies.size <= 1)
    Entry(name=parsedEntry.name,
          etymologySection=etymologies.headOption,
          posSections,
          rest)

extension (entry: Entry[String])
  def pronunciations: Seq[Pronunciation] =
    for
      pronunciationContent <- entry.remainingSections.filter(_.isPronunciation).flatMap(_.content)
      pronunciation <- PronunciationParser().parse(pronunciationContent)
    yield
      pronunciation

  def partsOfSpeech(using TextConverter): Seq[PartOfSpeechSection] =
    val senseParser = SenseParser()
    val headwordParser = HeadwordLineParser()
    given EntryContext(entry.name)

    for
      posSection <- entry.posSections
      posHeader <- posSection.heading
      posContent <- posSection.content
    yield
      PartOfSpeechSection(
        posHeader,
        headwordParser.parse(posContent),
        senseParser.parse(posContent),
        translations(posSection),
        usageNotes(posSection),
      )

  private def usageNotes(posSection: Section[String]): Option[String] =
    posSection.subsections
      .filter(_.isUsageNotes)
      .flatMap(_.content)
      .headOption

  private def translations(posSection: Section[String])
                          (using TextConverter)
                          (using EntryContext): Seq[TranslationGroup] =
    for
      translationContent <- posSection.subsections.filter(_.isTranslations).flatMap(_.content)
      group <- TranslationParser().parseTranslationSection(translationContent)
    yield
      group
