package digero.wiktionary.model

import digero.model.RawPage
import ipanema.links.Namespace

object Namespaces:
  private val namespaces = ipanema.links.Namespaces.load()

  val Main: Namespace = namespaces.get("")
  val Appendix: Namespace = namespaces.get("Appendix")
  val Module: Namespace = namespaces.get("Module")
  val Template: Namespace = namespaces.get("Template")
  val Reconstruction: Namespace = namespaces.get("Reconstruction")

  val Content: Seq[Namespace] = Seq(Main, Appendix, Reconstruction)

  def makeFilter(namespaces: Namespace*): ((String, RawPage)) => Boolean =
    (_: String, page: RawPage) => namespaces.exists(_.getId == page.namespace)