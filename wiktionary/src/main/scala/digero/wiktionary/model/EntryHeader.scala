package digero.wiktionary.model


case class EntryHeader[Content](
   sections: Seq[Section[Content]] = Seq.empty[Section[Content]],
   inherited: Seq[Section[Content]] = Seq.empty[Section[Content]],
   children: Seq[EntryHeader[Content]] = Seq.empty[EntryHeader[Content]]
):
  def contains(section: Section[Content]): Boolean =
    sections.contains(section)
    
  def allSections: Seq[Section[Content]] = sections ++ inherited  
  def headerSection: Section[Content] = sections.head

object EntryHeaders:
  def unapplySeq[T](sections: Seq[Section[T]]): Seq[EntryHeader[T]] =
    sections.partition(_.isGroupingHeader) match
      case (groupingHeaders @ Seq(_, _*), rest) =>
        groupingHeaders.map(section => section.subsections match
          case EntryHeaders(children*) =>
            EntryHeader(
              sections = section +: section.subsections.filterNot(section => children.exists(_.contains(section))),
              inherited = rest,
              children = children
            )
        )
      case (Nil, rest) if rest.forall(_.level == Level.L3) =>
        // no nesting, create new toplevel
        Seq(EntryHeader(rest.sorted))
      case _ =>
        // empty, no nesting
        Seq.empty

