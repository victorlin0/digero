package digero.wiktionary.model


case class TranslationSubpage(page: String, section: Option[String]):
  override def toString: String = page + section.map(s => s"#${s}").getOrElse("")

case class TranslationGroup(gloss: String,
                            translations: Seq[Translation],
                            subpage: Option[TranslationSubpage] = None)


case class Translation(language: String,
                       translation: String,
                       transliteration: Option[String],
                       script: String,
                       qualifier: Option[String],
                       needsVerifying: Boolean):

  def nonLatinScript: Option[String] = script match
    case "" | "Latn" => None
    case script => Some(script)
