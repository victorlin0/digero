package digero.wiktionary.model.lexical

// Semantic relations, see
// https://en.wiktionary.org/wiki/Wiktionary:Semantic_relations
enum RelationType:
  // Each listed synonym denotes the same as this entry.
  case Synonym // 0
  // Each listed antonym denotes the opposite of this entry.
  case Antonym
  // Each listed hypernym is superordinate to this entry.
  case Hypernym
  // Each listed hyponym is subordinate to this entry.
  case Hyponym
  // Each listed holonym has this entry’s referent as a part of itself.
  case Holonym
  // Each listed meronym denotes part of this entry’s referent.
  case Meronym
  // Each listed coordinate term shares a hypernym with this entry.
  case CoordinateTerm
  // Each listed troponym denotes a particular way to do this entry’s referent.
  case Troponym

  // Each listed “otherwise related” term semantically relates to this entry.
  case SeeAlso

  case DerivedTerm
  case RelatedTerm
  case Descendant

  case CharacteristicWordCombination // 12

  // Each listed comeronym shares this entry's referent as a holonym with another word or phrase.
  case Comeronym
  // Each listed parasynonym shares similar meanings with this entry's referent in some contexts, but not all.
  case NearSynonym

  case AlternativeForm


object RelationType:
  def apply(cssClass: String): RelationType = mapping(cssClass)

  private val mapping: PartialFunction[String, RelationType] =
      case "synonym" => RelationType.Synonym
      case "near-synonym" => RelationType.NearSynonym
      case "antonym" => RelationType.Antonym
      case "hypernym" => RelationType.Hypernym
      case "hyponym" => RelationType.Hyponym
      case "holonym" => RelationType.Holonym
      case "meronym" => RelationType.Meronym
      case "comeronym" => RelationType.Comeronym
      case "coordinate-term" => RelationType.CoordinateTerm
      case "troponym" => RelationType.Troponym
      case "alternative-form" => RelationType.AlternativeForm
