package digero.wiktionary.tasks.wanted

import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.functions.{broadcast, col, explode}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.io.File

/**
 * Finds "wanted' entries, entries that are linked to but don't exist at all or don't exist in the
 * language specified by the link.
 *
 * Needs the parsed HTML dump (produced by [[ParseHTMLDump]]) and a link
 * dataset (produced by [[CreateLinkGraph]]) as inputs.
 *
 * Outputs rows in the form of:
 *
 *          source sourceLanguage           target
 * 0  durchsickern             de    durchsickernd
 */
class FindWantedEntries extends DataFrameTask[File]:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.length >= 2)
    val output = File(arguments(1))
    require(output.getPath.nonEmpty)
    val links = session.read.parquet(arguments(0))
    val index = constructIndex(input)
    log.info(s"writing to $output")

    links.join(
        broadcast(index), // broadcast join
        (links.col("target") === index.col("entry")) &&
        (links.col("targetLanguage") === index.col("language")),
        joinType="left_anti"
      )
      .write
      .partitionBy("targetLanguage")
      .mode(SaveMode.Overwrite)
      .parquet(output.getAbsolutePath)

    output

  private def constructIndex(input: DataFrame): DataFrame =
    val index = input.select(col("entry"), col("language"), col("redirects"))

    // expand redirects to entry, [language]
    val redirects = index
      .filter(col("redirects").isNotNull)
      .select(
        explode(col("redirects")).as("entry"),
        col("language")
      )
    // combine with non-redirects
    redirects.union(
      index.select(col("entry"), col("language"))
    ).cache()
