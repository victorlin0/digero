package digero.wiktionary.tasks

import digero.mediawiki.Database.DEFAULT_ADMIN_GROUPS
import digero.mediawiki.{Database, DatabasePage}
import digero.model.RawPage
import digero.wikitext.SerializationSupport
import digero.wiktionary.model.Namespaces.{Appendix, Module, Template, makeFilter}
import digero.{RDDTask, TaskArguments}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import java.io.File
import scala.util.Using

/**
 * Expects RDD input of the XML dump and generates a SQLite with content from certain namespaces.
 * This SQLite database can then be dropped into a Mediawiki installation.
 */
class DatabaseImport extends RDDTask[File]:
  override def kryoRegistrator: String = classOf[SerializationSupport].getName

  def process(context: SparkContext, input: RDD[(String, RawPage)], arguments: TaskArguments): File =
    val output = arguments.outputFile

    Using.resource(new Database(output, true)) { db =>
      db.addUser("Admin", "admin", DEFAULT_ADMIN_GROUPS)
      val dbBroadcast = context.broadcast(db)
      input
        .coalesce(100)
        .filter(makeFilter(Template, Module, Appendix))
        .foreach((_, page: RawPage) => dbBroadcast.value.insert(new DatabasePage(page)))
    }
    output
