package digero.wiktionary.tasks.parse

import digero.spark.{getOpt, getSeqOpt}
import digero.wiktionary.AppSchema
import digero.wiktionary.parser.ParsoidHTMLParser
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.*
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.functions.col

import java.io.File
import java.time.Instant

/**
 * Extracts language sections from a Wiktionary HTML (Enterprise) Dump.
 */
class ParseHTMLDumpTask extends DataFrameTask[File]:

  private lazy val parser = ParsoidHTMLParser()

  given encoder: Encoder[Row] = ExpressionEncoder(AppSchema.parsed_html)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    log.info(s"ParseHTMLDump(${arguments})")
    require(arguments.length >= 1, "no arguments")

    val output = arguments.outputFile
    require(!output.getPath.isBlank, "need output dir") // Spark will happily trash the local directory :(

    (for
      row <- input
      content: String <- row.getOpt("content")
      title: String <- row.getOpt("title")
      entry <- parser.split(content)
      langCode <- entry.code.orElse {
        log.warn(s"unknown language '${entry.language}' in entry: ${title}")
        None
      }
      lastModified: Instant = row.getAs("last_modified")
      namespace: Int = row.getAs("namespace")
      redirects: Option[Seq[String]] = row.getSeqOpt("redirects")

    yield Row(title, namespace, langCode, entry.html, redirects.orNull, lastModified))
     .repartition(col("language"))
     .write
     .option("compression", "snappy")
     .option("header", "true")
     .partitionBy("language")
     .mode(SaveMode.Overwrite)
     .parquet(output.getAbsolutePath)

    output
