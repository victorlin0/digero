package digero.wiktionary.tasks.parse

import de.fau.cs.osr.ptk.common.AstVisitor
import digero.model.RawPage
import digero.wikitext.WtNodeExtensions.getText
import digero.wikitext.{ParsedPage, SerializationSupport, WikitextTask, WtNodeExtensions}
import digero.wiktionary.AppSchema
import digero.wiktionary.model.Namespaces.{Content, Main, makeFilter}
import digero.{RDDTask, TaskArguments}
import ipanema.language.model.LanguageData
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Row, SaveMode, SparkSession}
import org.sweble.wikitext.parser.nodes.{WtContentNode, WtNode, WtNodeList, WtSection}

import java.io.File
import scala.annotation.unused
import scala.collection.Iterable.single
import scala.collection.mutable.ListBuffer
import scala.jdk.OptionConverters.*

class ParseXMLDumpTask extends RDDTask[File]:

  override def kryoRegistrator: String = classOf[SerializationSupport].getName

  override def process(context: SparkContext, input: RDD[(String, RawPage)], arguments: TaskArguments): File =
    val output = arguments.outputFile
    require(!output.getPath.isBlank) // Spark will happily trash the local directory :(

    log.info(s"ParseXMLDump => $output")

    val textTask = WikitextTask()
    val broadcast = context.broadcast[LanguageData](LanguageData.load())
    val session = SparkSession.builder().config(context.getConf).getOrCreate()

    given serialization: SerializationSupport = SerializationSupport.get

    val contentFilter = makeFilter(Content: _*)

    val rdd = textTask.process(context, input.filter(contentFilter), TaskArguments.empty)
        .coalesce(100)
        .flatMapValues(page => getRows(page, broadcast.value))

    session
      .createDataFrame(rdd.values, AppSchema.parsed_xml)
      .repartition(col("namespace"), col("language"))
      .write
      .option("compression", "snappy")
      .partitionBy("namespace", "language")
      .mode(SaveMode.Overwrite)
      .parquet(output.getAbsolutePath)

    output

  private def getRows(page: ParsedPage, languageData: LanguageData)
                     (using serializer: SerializationSupport): IterableOnce[Row] =
    if page.rawTitle.namespace == Main.getId && page.rawTitle.redirect == null then
      for
        (code, section) <- collect(page.page)
        language <- languageData.get(code).toScala
        title = page.rawTitle.title
        namespace = page.rawTitle.namespace
        serialized = serializer.serialize(section)
        lastModified = page.timestamp.toInstant
        redirect = null
      yield Row(title, namespace, language.getCode, serialized, lastModified, redirect)
    else
      single(Row(
        page.rawTitle.title,
        page.rawTitle.namespace,
        null, // language
        serializer.serialize(page.page),
        page.timestamp.toInstant,
        page.rawTitle.redirect)
      )

  def collect(page: WtNodeList): Iterable[(String, WtContentNode)] =
    val visitor = CollectLevel2Headers()
    visitor.visit(page)
    visitor.languages

private class CollectLevel2Headers extends AstVisitor[WtNode]:
  private[wiktionary] val languages = ListBuffer[(String, WtContentNode)]()

  def visit(n: WtNodeList): Unit = iterate(n)
  def visit(@unused n: WtNode): Unit = { /* catch all */ }

  def visit(section: WtSection): Unit =
    section.getLevel match
      case 0 | 1 => iterate(section.getBody)
      case 2 =>
        for text <- section.getText
        do languages += text -> section.getBody
      case _ =>
