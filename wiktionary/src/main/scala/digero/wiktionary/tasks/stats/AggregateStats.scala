package digero.wiktionary.tasks.stats

import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.File

class AggregateStats extends DataFrameTask[File]:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.nonEmpty)
    val output = arguments.outputPath

    input.createOrReplaceTempView("stats")
    session.sql(
      s"""
         |SELECT
         |     language,
         |     type,
         |     array_contains(tags, 'NonLemma') as forms,
         |     count(*) as count
         |
         |   FROM stats
         |   GROUP BY language, forms, type
         |   ORDER BY count DESC, language
         |""".stripMargin
    ).write
     .json(output.toString)

    output.toFile
