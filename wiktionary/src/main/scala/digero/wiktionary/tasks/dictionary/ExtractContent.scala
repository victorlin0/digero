package digero.wiktionary.tasks.dictionary

import digero.spark.getOpt
import digero.wiktionary.model.serialization.{SerializedEntry, SerializedTranslationGroup, TextConverter}
import digero.wiktionary.model.{EntryGroup, EntryName, partsOfSpeech, pronunciations}
import digero.wiktionary.parser.{CommonMarkConverter, EntryParser}
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.functions.{col, not}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

class ExtractContent extends DataFrameTask[File]:
  private val parser = EntryParser()

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    val language = arguments.language.getOrElse("en")

    require(output.getPath.nonEmpty)
    given TextConverter = CommonMarkConverter().convert

    (for
      row <- input.filter(s"language = \"$language\"")
      html: String <- row.getOpt("html")
      entryName: EntryName <- Some(EntryName(row))
      entry <- EntryGroup(parser.parse(entryName, html))
      partOfSpeech <- entry.partsOfSpeech
      etymology = entry.etymologySection.flatMap(_.content)
      serializedEntry = SerializedEntry(entry.pageTitle, etymology, entry.pronunciations, partOfSpeech)
    yield serializedEntry)
      .write
      .mode(SaveMode.Overwrite)
      .option(JSONOptions.IGNORE_NULL_FIELDS, "false")
      .json(output.getAbsolutePath)

    output

class MergeTranslations extends DataFrameTask[File]:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    val translationsPageSuffix = "/translations"

    val isTranslationPage = col("pageTitle").endsWith(translationsPageSuffix)
    val translationEntries = input
      .where(isTranslationPage)
      .as[SerializedEntry]
      .collect()
      .toSeq
      .groupMap(_.pageTitle.replace(translationsPageSuffix, ""))(identity)

    val broadcast = session.sparkContext.broadcast(translationEntries)

    input
      .as[SerializedEntry]
      .map(entry => broadcast.value.get(entry.pageTitle) match
        case Some(translationEntries) => entry.mergeTranslations(translationEntries)
        case None => entry
      )
      .where(not(isTranslationPage))
      .write
      .mode(SaveMode.Overwrite)
      .option(JSONOptions.IGNORE_NULL_FIELDS, "false")
      .json(output.getAbsolutePath)

    output

extension (entry: SerializedEntry)
  def mergeTranslations(candidates: Seq[SerializedEntry]): SerializedEntry =
    val mergedTranslations: Seq[SerializedTranslationGroup] = entry
      .translations
      .flatMap(group => group.subpage match
        case Some(_) =>
          candidates
            .find(_.partOfSpeech == entry.partOfSpeech)
            .map(_.translations)
            .getOrElse(Seq(group))

        case None => Seq(group)
      )

    entry.copy(translations = mergedTranslations)