package digero.wiktionary.tasks.dictionary

import digero.wiktionary.model.serialization.SerializedTranslationGroup
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.functions.{array_size, col}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

class ExtractTranslations extends DataFrameTask[File]:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    val language = arguments.language.get
    require(output.getPath.nonEmpty)

    val (pageTitle, partOfSpeech, translations) = (col("pageTitle"), col("partOfSpeech"), col("translations"))

    input.select(pageTitle, partOfSpeech, translations)
      .where(array_size(translations) > 0)
      .as[(String, String, Seq[SerializedTranslationGroup])]
      .map { case (page, pos, groups) =>
        (page, pos, groups.map(group => group.copy(translations = group.translations.filter(_.language == language))))
      }
      .map { case (page, pos, groups) => (page, pos, groups.filter(_.translations.nonEmpty)) }
      .filter { case (_, _, groups) => groups.nonEmpty }
      .write
      .mode(SaveMode.Overwrite)
      .json(output.getAbsolutePath)

    output


