package digero.wiktionary.tasks.dictionary

import digero.wiktionary.model.serialization.SerializedEntry
import digero.wiktionary.parser.TranslationMatcher
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

class MatchTranslations extends DataFrameTask[File]:

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile

    input
      .as[SerializedEntry]
      .map(entry => transform(entry).getOrElse(entry))
      .write
      .mode(SaveMode.Overwrite)
      .option(JSONOptions.IGNORE_NULL_FIELDS, "false")
      .json(output.getPath)

    output

  private inline def transform(serializedEntry: SerializedEntry): Option[SerializedEntry] =
    TranslationMatcher.matchEntry(serializedEntry)
