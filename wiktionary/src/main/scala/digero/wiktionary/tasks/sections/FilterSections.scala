package digero.wiktionary.tasks.sections

import digero.Task.copyMerge
import digero.wiktionary.model.{EntryName, SectionHeading}
import digero.wiktionary.parser.EntryParser
import digero.{DataFrameTask, TaskArguments}
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.SaveMode.Overwrite
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.types.{StringType, StructType}
import org.apache.spark.sql.{DataFrame, Encoder, Row, SparkSession}

import java.io.File

class FilterSections extends DataFrameTask[File]:
  private val parser = EntryParser()

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    require(output.getPath.nonEmpty)
    val outputWork = File(output.getParentFile, output.getName + ".work")

    given encoder: Encoder[Row] = ExpressionEncoder((new StructType)
      .add("entry", StringType)
      .add("language", StringType)
    )

    input.flatMap(row =>
      val entry = EntryName(row)
      val html = row.getAs[String]("html")

      val parsed = parser.parse(entry, html)

      parsed.sections.flatMap(_.withChildren)
        .filter(_.heading match
          case Some(SectionHeading.DerivedSigns) => true
          case _ => false
        )
        .map(_ => Row(entry.name, entry.languageCode))
    ).write
     .mode(Overwrite)
     .json(outputWork.getAbsolutePath)

    copyMerge(FileSystem.get(session.sparkContext.hadoopConfiguration), srcDir=outputWork, output)

    output
