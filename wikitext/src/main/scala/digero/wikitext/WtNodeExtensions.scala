package digero.wikitext

import org.sweble.wikitext.parser.nodes.{WtNode, WtText}

object WtNodeExtensions:
  extension (node: WtNode)
    def getText: Option[String] =
      node match
        case text: WtText => text.getText
        case _ if !node.isEmpty => node.get(0).getText
        case _ => None
  
  extension (textNode:WtText)
    /**
     * @return non-empty string with leading and trailing whitespace removed, or empty
     */
    def getText: Option[String] =
      val content = textNode.getContent.trim
      if content.isEmpty
      then None
      else Some(content)
