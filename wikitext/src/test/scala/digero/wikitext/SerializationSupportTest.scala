package digero.wikitext

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{BeforeEach, Test}
import org.sweble.wikitext.engine.nodes.EngPage
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp
import org.sweble.wikitext.engine.{PageId, PageTitle, WtEngineImpl}
import org.sweble.wikitext.parser.nodes.{WtContentNode, WtTemplate, WtTemplateArgument}
import org.sweble.wikitext.parser.utils.WtPrettyPrinter

class SerializationSupportTest:
  private var subject: SerializationSupport = _

  @BeforeEach def setUp(): Unit =
    subject = new SerializationSupport

  @Test
  def testRoundTrip(): Unit =
    val result = roundTrip("==Test==\n\n# {{m|en|foo=bar}}\n# bar")
    assertThat(result).isInstanceOf(classOf[EngPage])
    assertThat(result).hasSize(1)
    assertThat(WtPrettyPrinter.print(result)).isEqualTo("\n==Test==\n\n# {{m|en|foo=bar}}\n# bar")

  @Test
  def testNoNameDeserialization(): Unit =
    val result = roundTrip("{{a|b}}")
    assertThat(result.get(0).get(0)).isInstanceOf(classOf[WtTemplate])
    val template = result.get(0).get(0).asInstanceOf[WtTemplate]
    val args = template.getArgs
    assertThat(args).hasSize(1)
    val argument = args.get(0).asInstanceOf[WtTemplateArgument]
    assertThat(argument.hasName).isFalse

  private def roundTrip(wikiText: String): WtContentNode =
    val page = parse(wikiText)
    val serialized = subject.serialize(page)
    subject.deserialize(serialized)

  private def parse(wikiText: String): EngPage =
    val wikiConfig = DefaultConfigEnWp.generate
    val wtEngine = new WtEngineImpl(wikiConfig)
    val pageId = new PageId(PageTitle.make(wikiConfig, "test"), 0)
    val page = wtEngine.postprocess(pageId, wikiText, null)
    page.getPage
