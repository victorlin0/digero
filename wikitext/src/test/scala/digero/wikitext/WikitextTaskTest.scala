package digero.wikitext

import digero.TaskArguments
import digero.model.RawPage
import digero.model.RawPage.CONTENT_MODEL_WIKITEXT
import org.apache.spark.{SparkConf, SparkContext}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{BeforeEach, Tag, Test}
import org.sweble.wikitext.parser.utils.WtPrettyPrinter

@Tag("integration")
class WikitextTaskTest:
  private var testConf: SparkConf = _

  @BeforeEach def setUp(): Unit =
    testConf = new SparkConf(false)
        .setMaster("local")
        .set("spark.kryo.registrator", classOf[SerializationSupport].getName)
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        .setAppName(getClass.getCanonicalName)

  @Test def testProcessDataset(): Unit =
      val context = new SparkContext(testConf)

      val wikitextTask = new WikitextTask
      val page = new RawPage
      page.contentModel = CONTENT_MODEL_WIKITEXT
      page.title = "title"
      page.text = "foo"

      val rdd = context.parallelize(Seq(page.title -> page))
      val result = wikitextTask.process(context, rdd, TaskArguments.empty)
      val parsedPage = result.first()._2

      assertThat(parsedPage.rawTitle.title).isEqualTo("title")
      assertThat(parsedPage.page.getPropertyCount).isEqualTo(1)
      assertThat(WtPrettyPrinter.print(parsedPage.page)).isEqualTo("foo")
