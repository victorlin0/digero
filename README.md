# dīgerō

Process [MediaWiki data dumps][] (HTML & XML) using [Spark][]. Works efficiently on compressed
data dumps by performing the decompression in parallel (XML dumps only).

## Components

The project is split into several modules:

 * **model**: Basic model classes, no dependencies
 * **core**: Spark Data Sources to parse [MediaWiki data dumps][] and
  CBOR formatted data.
 * **mediawiki** MediaWiki database creation (SQLite)
 * **wikitext**: [MediaWiki markup][] parsing
 * **wiktionary**:  Wiktionary-specific tasks

There is a separate [README.md](/wiktionary/README.md) which documents Wiktionary maintenance
jobs (statistics, wanted entries etc.).

[Spark]: https://spark.apache.org/
[MediaWiki data dumps]: https://meta.wikimedia.org/wiki/Data_dumps
[MediaWiki markup]: https://www.mediawiki.org/wiki/Help:Formatting

