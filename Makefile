DUMP_VERSION=20230701

PANTS=pants
CURL_OPTS=--fail --location --continue-at -
PYTHON_SRC_DIR 	   = wiktionary/src/main/python/digero

# dumps
DUMPS_DIR          = wiktionary/dumps
XML_DUMP_BZ2 	   = $(DUMPS_DIR)/enwiktionary-$(DUMP_VERSION)-pages-articles-multistream.xml.bz2
XML_DUMP_INDEX_BZ2 = $(DUMPS_DIR)/enwiktionary-$(DUMP_VERSION)-pages-articles-multistream-index.txt.bz2

XML_DUMP_BZ2_URL       = https://dumps.wikimedia.org/enwiktionary/$(DUMP_VERSION)/enwiktionary-$(DUMP_VERSION)-pages-articles-multistream.xml.bz2
XML_DUMP_INDEX_BZ2_URL = https://dumps.wikimedia.org/enwiktionary/$(DUMP_VERSION)/enwiktionary-$(DUMP_VERSION)-pages-articles-multistream-index.txt.bz2

HTML_DUMP_TGZ      = $(DUMPS_DIR)/enwiktionary-NS0-$(DUMP_VERSION)-ENTERPRISE-HTML.json.tar.gz
HTML_DUMP_URL 	   = https://dumps.wikimedia.org/other/enterprise_html/runs/$(DUMP_VERSION)/enwiktionary-NS0-$(DUMP_VERSION)-ENTERPRISE-HTML.json.tar.gz

# outputs
BASE_DIR         = wiktionary/parsed/$(DUMP_VERSION)
DICTIONARY_DIR   = dist/dictionaries
HTML_DUMP        = $(BASE_DIR)/dump-html.parquet
HTML_DUMP_RAW    = $(BASE_DIR)/dump-html-raw.parquet
HTML_DUMP_CLEAN  = $(BASE_DIR)/dump-html-clean.parquet
XML_DUMP         = $(BASE_DIR)/dump-xml.parquet
XML_DUMP_RAW     = $(BASE_DIR)/dump-xml-raw.parquet
LINKS 	         = $(BASE_DIR)/links.parquet
WANTED_ENTRIES   = $(BASE_DIR)/wanted-entries.parquet
AGGREGATED_DIR 	 = $(BASE_DIR)/aggregated
MISPLACED_LABELS = $(BASE_DIR)/misplaced-labels.jsonl

parse: $(HTML_DUMP)
links: $(LINKS)
wanted_entries: $(WANTED_ENTRIES)

$(HTML_DUMP): $(HTML_DUMP_CLEAN)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.parse.ParseHTMLDumpTask $< $@

$(HTML_DUMP_CLEAN): $(HTML_DUMP_RAW)
	$(PANTS) run core:run_data_frame_task -- CleanHTMLDumpTask $< $@

$(HTML_DUMP_RAW): $(HTML_DUMP_TGZ)
	$(PANTS) run core:process_html_dump -- ImportHTMLDumpTask $< $@

$(LINKS): $(HTML_DUMP)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.wanted.CreateLinkGraph $< $@

$(WANTED_ENTRIES): $(HTML_DUMP) $(LINKS)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.wanted.FindWantedEntries $^ $@

aggregate_wanted_entries: $(AGGREGATED_DIR)
$(AGGREGATED_DIR): $(WANTED_ENTRIES)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.wanted.AggregateWantedEntries $^ $@

publish_wanted_entries: user-config.py $(AGGREGATED_DIR)
	$(PANTS) run $(PYTHON_SRC_DIR)/publish/publish_wanted_entries.py -- $(AGGREGATED_DIR)

$(XML_DUMP): $(XML_DUMP_BZ2) $(XML_DUMP_INDEX_BZ2)
	$(PANTS) run wiktionary:process_xml_dump -- digero.wiktionary.tasks.parse.ParseXMLDumpTask $(XML_DUMP_BZ2),$(XML_DUMP_INDEX_BZ2) $@

$(XML_DUMP_RAW): $(XML_DUMP_BZ2) $(XML_DUMP_INDEX_BZ2)
	$(PANTS) run core:process_xml_dump -- ImportXMLDumpTask $(XML_DUMP_BZ2),$(XML_DUMP_INDEX_BZ2) $@

$(MISPLACED_LABELS): $(XML_DUMP)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.FindMisplacedLabels $< $@

publish_misplaced_labels: $(MISPLACED_LABELS)
	$(PANTS) run $(PYTHON_SRC_DIR)/publish/publish_misplaced_labels.py -- $<

$(BASE_DIR)/enwiktionary.sqlite: $(XML_DUMP_BZ2) $(XML_DUMP_INDEX_BZ2)
	$(PANTS) run wiktionary:process_xml_dump -- digero.wiktionary.tasks.DatabaseImport $(XML_DUMP_BZ2),$(XML_DUMP_INDEX_BZ2) $@

$(BASE_DIR)/index-html.txt: $(HTML_DUMP_RAW)
	$(PANTS) run core:run_data_frame_task -- digero.tasks.CreateIndexFromHTMLDumpTask $< $@

$(BASE_DIR)/content_%.ndjson: $(HTML_DUMP)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.dictionary.ExtractContent $< $@ $(patsubst content_%.ndjson,%,$(@F))

$(BASE_DIR)/merged_en.ndjson: $(BASE_DIR)/content_en.ndjson
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.dictionary.MergeTranslations $< $@

$(BASE_DIR)/matched_en.ndjson: $(BASE_DIR)/merged_en.ndjson
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.dictionary.MatchTranslations $< $@

$(BASE_DIR)/translations_%.ndjson: $(BASE_DIR)/merged_en.ndjson
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.dictionary.ExtractTranslations $< $@ $(patsubst translations_%.ndjson,%,$(@F))

.PRECIOUS: $(BASE_DIR)/content_%.ndjson $(BASE_DIR)/translations_%.ndjson $(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-%-no-trans.sqlite
$(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-%-no-trans.sqlite: $(BASE_DIR)/content_%.ndjson
	$(PANTS) run $(PYTHON_SRC_DIR)/dictionary/make_dictionary.py -- \
		--language $(patsubst wiktionary-$(DUMP_VERSION)-%-no-trans.sqlite,%,$(@F)) \
		--dump-version $(DUMP_VERSION) \
		--out $@ \
		$</*.json

# special case for English, no translations needed
$(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-en.sqlite: $(BASE_DIR)/content_en.ndjson
	$(PANTS) run $(PYTHON_SRC_DIR)/dictionary/make_dictionary.py -- \
		--language $(patsubst wiktionary-$(DUMP_VERSION)-%.sqlite,%,$(@F)) \
		--dump-version $(DUMP_VERSION) \
		--index \
		--out $@ \
		$</*.json

.PRECIOUS: $(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-%.sqlite
$(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-%.sqlite: $(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-%-no-trans.sqlite $(BASE_DIR)/translations_%.ndjson
	cp $< $@
	$(PANTS) run $(PYTHON_SRC_DIR)/dictionary/add_translations.py -- \
		--index \
		--db $@ \
		$(filter-out $<,$^)/*.json

$(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-%.sqlite.lzma: $(DICTIONARY_DIR)/wiktionary-$(DUMP_VERSION)-%.sqlite
	lzma --compress --stdout $< > $@

$(BASE_DIR)/missing-sections.ndjson: $(HTML_DUMP)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.sections.AnalyzeMissingSections $< $@

$(BASE_DIR)/header-sections.ndjson: $(HTML_DUMP)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.sections.AnalyzeSectionHeaderNesting $< $@

$(BASE_DIR)/filtered_sections.ndjson: $(HTML_DUMP)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.sections.FilterSections $< $@

$(BASE_DIR)/stats.ndjson: $(HTML_DUMP)
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.stats.ExtractStats $< $@ $(FILTER)

$(BASE_DIR)/aggregated_stats.ndjson: $(BASE_DIR)/stats.ndjson
	$(PANTS) run wiktionary:run_data_frame_task -- digero.wiktionary.tasks.stats.AggregateStats $< $@

clean:
	rm -rf $(WANTED_ENTRIES) $(AGGREGATED_DIR) $(LINKS) $(INDEX)

$(HTML_DUMP_TGZ):
	curl $(HTML_DUMP_URL) $(CURL_OPTS) --output $@

$(XML_DUMP_BZ2):
	curl $(XML_DUMP_BZ2_URL) $(CURL_OPTS) --output $@

$(XML_DUMP_INDEX_BZ2):
	curl $(XML_DUMP_INDEX_BZ2_URL) $(CURL_OPTS) --output $@

# placeholder user-config.py for pywikibot
# https://meta.wikimedia.org/wiki/Special:OAuthConsumerRegistration/propose
define config
family = 'wiktionary'
mylang = 'en'

usernames['wiktionary']['en'] = 'username'
authenticate['en.wiktionary.org'] = ('consumer token',
                                     'consumer_secret',
                                     'access_token',
                                     'access_secret')
endef
export config

user-config.py:
	echo "$$config" > $@
