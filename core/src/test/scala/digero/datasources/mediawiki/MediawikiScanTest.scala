package digero.datasources.mediawiki

import digero.test.Fixture
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.execution.datasources.{FilePartition, InMemoryFileIndex}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.util.CaseInsensitiveStringMap
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.api.{AfterAll, BeforeAll, Tag, Test, TestInstance}

import scala.jdk.CollectionConverters.*

@Tag("integration")
@TestInstance(PER_CLASS)
class MediawikiScanTest extends Fixture:
  var session: SparkSession = _

  @BeforeAll
  def beforeAll(): Unit = session = SparkSession.builder().master("local").getOrCreate()

  @AfterAll
  def afterAll(): Unit = session.stop()

  @Test
  def testPlanInputPartitions(): Unit =
    val paths = Seq(
      fixturePath("enwiktionary-20150224-pages-articles-multistream.xml.bz2"),
      fixturePath("enwiktionary-20150224-pages-articles-multistream-index.txt.bz2")
    ).map(file => new Path(file))

    val fileIndex = new InMemoryFileIndex(session, paths, Map.empty, Option.empty)
    val schema = new StructType()
    val options = CaseInsensitiveStringMap.empty()
    val subject = MediawikiScan(session, fileIndex, schema, schema, schema, options)
    val partitions = subject.planInputPartitions()

    val results = for
      case partition: FilePartition <- partitions
      file <- partition.files
    yield (partition.index, file.start, file.length)

    assertThat(results).isEqualTo(Array(
      (0, 654, 260719),
      (1, 261373,126199),
      (2, 387572, 57551)
    ))

  @Test
  def testPlanInputPartitionsGroupSize(): Unit =
    val paths = Seq(
      fixturePath("enwiktionary-20150224-pages-articles-multistream.xml.bz2"),
      fixturePath("enwiktionary-20150224-pages-articles-multistream-index.txt.bz2")
    ).map(file => new Path(file))

    val fileIndex = new InMemoryFileIndex(session, paths, Map.empty, Option.empty)
    val schema = new StructType()
    val options = CaseInsensitiveStringMap(Map("groupSize" -> "2").asJava)
    val subject = MediawikiScan(session, fileIndex, schema, schema, schema, options)
    val partitions = subject.planInputPartitions()

    val results = for
      case partition: FilePartition <- partitions
      file <- partition.files
    yield (partition.index, file.start, file.length)

    assertThat(results).isEqualTo(Array(
      (0, 654, 260719),
      (0, 261373,126199),
      (1, 387572, 57551)
    ))
