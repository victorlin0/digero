package digero.rdd

import digero.test.Fixture
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat.INPUT_DIR
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import java.io.File
import scala.jdk.CollectionConverters.*

object MediawikiDumpInputFormatTest:
  private val MULTISTREAM = "enwiktionary-20150224-pages-articles-multistream.xml.bz2"
  private val MULTISTREAM_INDEX = "enwiktionary-20150224-pages-articles-multistream-index.txt.bz2"

class MediawikiDumpInputFormatTest extends Fixture:
  private var index: File = _
  private var dump: File = _

  @BeforeEach def setUp(): Unit =
    dump = fixtureFile(MediawikiDumpInputFormatTest.MULTISTREAM)
    index = fixtureFile(MediawikiDumpInputFormatTest.MULTISTREAM_INDEX)

  @Test
  def testGetSplits(): Unit =
    val job = Job.getInstance
    job.getConfiguration.set(INPUT_DIR, dump.toString + "," + index)
    val subject = new MediawikiDumpInputFormat
    
    subject.getSplits(job)
      .asScala
      .map(_.toString)
      .map(s => s.substring(s.lastIndexOf("/") + 1)) shouldEqual Seq(
        "enwiktionary-20150224-pages-articles-multistream.xml.bz2:654+260719",
        "enwiktionary-20150224-pages-articles-multistream.xml.bz2:261373+126199",
        "enwiktionary-20150224-pages-articles-multistream.xml.bz2:387572+57551"
    )
