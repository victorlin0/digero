package digero.model

import org.junit.jupiter.api.{BeforeEach, Test}

import org.scalatest.matchers.should.Matchers.*
import digero.model.TemplateParameterMap._

class TemplateTest:
  var subject: Template = _

  @BeforeEach def setUp(): Unit =
    val parameters = TemplateParameterMap()
    parameters.put(1, "Foo")
    parameters.put(2, "Bar")
    parameters.put("key", "value")
    subject = Template("test", parameters)

  @Test def testGetParameterByIntKey(): Unit =
    subject.getParameter(1) should contain("Foo")
    subject.getParameter(2) should contain("Bar")
    subject.getParameter(3) shouldBe empty

  @Test def testGetByParameterStringKey(): Unit =
    subject.getParameter("2") shouldBe empty
    subject.getParameter("key") should contain("value")

  @Test def testGetAnonymousParameters(): Unit =
    val parameters = TemplateParameterMap()
    parameters.put(5, "Foo")
    parameters.put("key", "value")
    parameters.put(10, "Baz")
    parameters.put(3, "Bar")
    val template = Template("test", parameters)

    template.anonymousParameters should contain inOrderOnly("Bar", "Foo", "Baz")

  @Test def testAssumesOneBasedIndex(): Unit =
    an [AssertionError] should be thrownBy subject.getParameter(0)

  @Test def testConstructFromParametersWithGaps(): Unit =
    val parameters = TemplateParameterMap()
    parameters.put(1, "Foo")
    parameters.put(3, "Bar")
    val template = new Template("test", parameters)
    template.getParameter(1) should contain("Foo")
    template.getParameter(2) shouldBe empty
    template.getParameter(3) should contain("Bar")
