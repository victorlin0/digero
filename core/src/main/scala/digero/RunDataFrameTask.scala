package digero

import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.File
import scala.util.Using

/**
 * Runner class for a [[DataFrameTask]], using a previously parsed dump.
 * See [[digero.ProcessHTMLDump]] and [[digero.ProcessXMLDump]] for the runner class to process a dump.
 */
@main
def RunDataFrameTask(className: String, file: String, rest: String*): Unit =
  val task: DataFrameTask[_] = Task.getTask(className)
  RunDataFrameTask(task, new File(file), rest)

private def RunDataFrameTask(task: DataFrameTask[_], file: File, rest: Seq[String]): Unit =
  Using.resource(SparkRunner(task)) { runner =>
    task.process(runner.session, getDataFrame(runner.session, file), TaskArguments(rest))
  }

private def getDataFrame(session: SparkSession, file: File): DataFrame =
  if file.getName.endsWith(".json") ||
    file.getName.endsWith(".jsonl") ||
    file.getName.endsWith(".ndjson") then

    if file.isDirectory then
      val paths = file.listFiles()
        .filterNot(_.getName.startsWith("."))
        .filterNot(_.getName.startsWith("_"))
        .map(_.getAbsolutePath)

      session.read.json(paths: _*)
    else
      session.read.json(file.getAbsolutePath)
  else
    session.read.parquet(file.getAbsolutePath)
