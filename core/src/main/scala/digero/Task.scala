package digero

import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileUtil.fullyDelete
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.IOUtils

import java.io.{File, IOException}
import scala.util.Using

/**
 * Base trait for tasks.
 */
trait Task extends Serializable:
  def kryoRegistrator: String = null

  val log: Log = LogFactory.getLog(this.getClass)

object Task:
  private val DEFAULT_PREFIX = "digero.tasks."
  private val MIN_MEMORY =  2 * 1024 * 1024 * 1024L // 2GiB

  def checkMemory(min: Long = MIN_MEMORY): Unit =
    val maxMemory = Runtime.getRuntime.maxMemory()
    require(maxMemory >= MIN_MEMORY, "not enough memory to run: %d, need %d".formatted(maxMemory, min))

  def getTask[TaskT <: Task](name: String): TaskT =
    try instantiate[TaskT](name)
    catch case _: ClassNotFoundException => instantiate[TaskT](DEFAULT_PREFIX + name)

  private def instantiate[TaskT <: Task](name: String): TaskT =
    Class.forName(name).getDeclaredConstructor().newInstance().asInstanceOf[TaskT]

  def copyMerge(fs: FileSystem, srcDir: File, dstFile: File): Boolean =
    fullyDelete(dstFile)
    copyMerge(fs, new Path(srcDir.getAbsolutePath), fs, new Path(dstFile.getAbsolutePath), fs.getConf)

  // Deprecated in Hadoop 3.0
  // https://stackoverflow.com/questions/42035735/how-to-do-copymerge-in-hadoop-3-0
  private def copyMerge(srcFS: FileSystem, srcDir: Path, dstFS: FileSystem, dstFile: Path, conf: Configuration): Boolean =
    if dstFS.exists(dstFile) then
      throw new IOException("File already exists")

    if !srcFS.getFileStatus(srcDir).isDirectory then
      return false

    Using.resource(dstFS.create(dstFile)) { out =>
      for
        content <- srcFS.listStatus(srcDir).sorted
        if content.isFile
      do
        Using.resource(srcFS.open(content.getPath)) { in =>
          IOUtils.copyBytes(in, out, conf, false)
        }
    }
    srcFS.delete(srcDir, true)
