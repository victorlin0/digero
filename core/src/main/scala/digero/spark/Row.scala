package digero.spark

import org.apache.spark.sql.Row

extension (row: Row)
  def getSeqOpt[T](fieldName: String): Option[Seq[T]] =
    row.getOpt[scala.collection.Seq[T]](fieldName).map(_.toSeq)

  def getOpt[T](fieldName: String): Option[T] =
    if row.isNullAt(row.fieldIndex(fieldName)) then None else Some(row.getAs[T](fieldName))
