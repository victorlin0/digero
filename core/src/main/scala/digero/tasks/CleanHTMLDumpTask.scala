package digero.tasks

import digero.{DataFrameTask, HTMLDumpSchema, TaskArguments}
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.{DataFrame, Encoder, Row, SaveMode, SparkSession}

import java.io.File

class CleanHTMLDumpTask extends DataFrameTask[File]:

  given encoder: Encoder[Row] = ExpressionEncoder(HTMLDumpSchema.simplifiedOutput)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    log.info(s"CleanHTMLDump($arguments)")
    require(arguments.length >= 1, "no arguments")

    val output = arguments.outputFile
    require(!output.getPath.isBlank, "need output dir") // Spark will happily trash the local directory :(

    input
      .dropDuplicates("title")
      .write
      .mode(SaveMode.Overwrite)
      .option("compression", "snappy")
      .option("header", "true")
      .parquet(output.getPath)

    output
