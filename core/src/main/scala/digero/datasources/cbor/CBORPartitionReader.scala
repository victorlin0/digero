package digero.datasources.cbor

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.{JsonNodeType, ObjectNode}
import com.fasterxml.jackson.dataformat.cbor.CBORFactory
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper
import org.apache.hadoop.conf.Configuration
import org.apache.spark.paths.SparkPath
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.expressions.GenericInternalRow
import org.apache.spark.sql.catalyst.util.{ArrayBasedMapData, ArrayData, MapData}
import org.apache.spark.sql.connector.read.PartitionReader
import org.apache.spark.sql.types.*
import org.apache.spark.unsafe.types.UTF8String

import java.io.{IOException, InputStream}
import scala.jdk.CollectionConverters.*

class CBORPartitionReader(private val path: SparkPath, private val schema: StructType) extends PartitionReader[InternalRow]:
  final private val cborMapper = new CBORMapper
  private var cborParser: JsonParser = _
  private var nextObject: JsonNode = _

  @throws[IOException]
  override def next: Boolean =
    if (cborParser == null) {
      val stream = path.toPath.getFileSystem(new Configuration).open(path.toPath)
      cborParser = new CBORFactory().createParser(stream.asInstanceOf[InputStream])
      cborParser.enable(AUTO_CLOSE_SOURCE)
    }
    nextObject = cborMapper.readTree(cborParser)
    nextObject != null

  override def get: InternalRow =
    if (schema.isEmpty)
      new GenericInternalRow(size = 0)
    else
      toRow(nextObject, schema)

  @throws[IOException]
  override def close(): Unit = cborParser.close()

  private def toRow(node: JsonNode, rowSchema: StructType): InternalRow =
    val row = new GenericInternalRow(rowSchema.size)
    for (i <- rowSchema.indices) {
      val field = rowSchema.apply(i)
      row.update(i, convert(node.get(field.name), field.dataType))
    }
    row

  private def convert(jsonNode: JsonNode, dataType: DataType): Object =
      jsonNode.getNodeType match {
        case JsonNodeType.ARRAY =>
          dataType match
            case arrayType: ArrayType => convertToArrayData(jsonNode, arrayType)
            case _ => throw new UnsupportedOperationException("unsupported type " + dataType)

        case JsonNodeType.OBJECT =>
          dataType match
            case _: MapType => convertToMapData(jsonNode)
            case structType: StructType => toRow(jsonNode, structType)
            case _ => throw new UnsupportedOperationException("unsupported type " + dataType)

        case _ => convertPrimitive(jsonNode)
      }

  private def convertPrimitive(jsonNode: JsonNode): Object =
      jsonNode.getNodeType match {
        case JsonNodeType.BOOLEAN =>
          Boolean.box(jsonNode.booleanValue)
        case JsonNodeType.NUMBER =>
          jsonNode.numberValue
        case JsonNodeType.STRING =>
          UTF8String.fromString(jsonNode.asText)
        case JsonNodeType.NULL =>
          null
        case _ =>
          throw new UnsupportedOperationException("not supported: " + jsonNode.getNodeType)
      }

  private def convertToMapData(jsonNode: JsonNode): MapData =
      assert(jsonNode.getNodeType eq JsonNodeType.OBJECT)
      val objectNode = jsonNode.asInstanceOf[ObjectNode]

      val keysArray = objectNode.fieldNames().asScala.map(UTF8String.fromString).toArray
      val keysArrayData = ArrayData.toArrayData(keysArray)
      val valuesArray = objectNode.elements().asScala.map(convertPrimitive).toArray
      val valuesArrayData = ArrayData.toArrayData(valuesArray)
      new ArrayBasedMapData(keysArrayData, valuesArrayData)

  private def convertToArrayData(jsonNode: JsonNode, arrayType: ArrayType): ArrayData =
      assert(jsonNode.getNodeType eq JsonNodeType.ARRAY)
      val objs = new Array[AnyRef](jsonNode.size)
      for (i <- 0 until jsonNode.size) {
        objs(i) = convert(jsonNode.get(i), arrayType.elementType)
      }
      ArrayData.toArrayData(objs)
