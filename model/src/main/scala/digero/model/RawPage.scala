package digero.model

import java.io.Serializable
import java.time.OffsetDateTime
import javax.annotation.Nullable
import scala.beans.BeanProperty

@SerialVersionUID(7909355701674974804L)
object RawPage:
  var CONTENT_MODEL_WIKITEXT = "wikitext"

@SerialVersionUID(7909355701674974804L)
class RawPage extends Serializable:
  // TODO: everything be immutable
  @BeanProperty var namespace = 0
  @BeanProperty var title: String = _
  @BeanProperty var text: String = _
  @BeanProperty @Nullable var redirect: String = _
  @BeanProperty var timestamp: OffsetDateTime = _

  /**
   * The content model for this page.
   * Usually one of wikitext|text|css|javascript|json|Scribunto
   *
   * @see <a href="https://www.mediawiki.org/wiki/Content_handlers">Content handlers</a>
   */
  @BeanProperty var contentModel: String = _
  @BeanProperty var id: Long = 0L
  @BeanProperty var revisionId: Long = 0L
  @BeanProperty var contributorId = 0L

  def isEmpty: Boolean = (title == null || title.isEmpty) || (text == null || text.isEmpty)

  override def toString: String =
    s"RawPage{namespace=$namespace, title='$title', text='$text', redirect='$redirect', " +
      s"contentModel='$contentModel', revisionId=$revisionId, contributorId=$contributorId}"
