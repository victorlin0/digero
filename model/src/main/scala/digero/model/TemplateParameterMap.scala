package digero.model

type TemplateParameter = Either[Int, String]
type TemplateParameterMap = collection.mutable.LinkedHashMap[TemplateParameter, String]

object TemplateParameterMap:
  def apply(): TemplateParameterMap = collection.mutable.LinkedHashMap[TemplateParameter, String]()
  def apply(values: Seq[(TemplateParameter, String)]): TemplateParameterMap =
      collection.mutable.LinkedHashMap[TemplateParameter, String](values: _*)

  extension (map: TemplateParameterMap)
    def put(string: String, value: String): Option[String] = map.put(Right(string), value)
    def put(index: Int, value: String): Option[String] = map.put(Left(index), value)
