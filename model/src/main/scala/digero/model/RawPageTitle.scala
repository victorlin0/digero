package digero.model

import java.io.Serializable

@SerialVersionUID(-6989585424832287817L)
class RawPageTitle(val title: String, val redirect: String, val namespace: Int) extends Serializable:
  override def toString: String =
    s"RawPageTitle{title='$title', redirect='$redirect', namespace=$namespace}"
